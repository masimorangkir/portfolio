/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gii.service;
import java.math.BigDecimal;
//import org.springframework.stereotype;
/**
 *
 * @author Genesys8
 */

public interface IMath {
    public BigDecimal additionOperation(BigDecimal first, BigDecimal second);
    public BigDecimal substractOperation(BigDecimal first, BigDecimal second);
}
