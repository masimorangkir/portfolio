/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gii.service;
import com.gii.model.SourceDto;
/**
 *
 * @author Genesys8
 */
public interface ISourceService {
    public SourceDto findByName(String nameToFind);
    public String toStringJdbc();
}
