/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gii.service.impl;

import com.gii.service.IMath;
import java.math.BigDecimal;
import org.springframework.stereotype.Component;
/**
 *
 * @author Genesys8
 */
@Component
public class CalculatorImpl implements IMath{

    @Override
    public BigDecimal additionOperation(BigDecimal first, BigDecimal second) {
        return first.add(second);
    }

    @Override
    public BigDecimal substractOperation(BigDecimal first, BigDecimal second) {
        return first.subtract(second);
    }
    
}
