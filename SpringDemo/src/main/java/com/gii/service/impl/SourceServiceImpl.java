/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gii.service.impl;

import com.gii.model.SourceDto;
import com.gii.service.ISourceService;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.stereotype.Component;
//import sun.jdbc.odbc.ee.DataSource;

/**
 *
 * @author Genesys8
 */
@Component
public class SourceServiceImpl implements ISourceService{
   @Autowired
    private JdbcTemplate jdbc;
    
   /* @Autowired
    public void setDataSource(DataSource source){
        this.jdbc = new JdbcTemplate(source);
    }
    */
    @Override
    public SourceDto findByName(String nameToFind) {
        List<SourceDto> dto = this.jdbc.query("SELECT name FROM source where id=?", ParameterizedBeanPropertyRowMapper.newInstance(SourceDto.class),nameToFind);
       
        if(dto.size()>0){
            return dto.get(0);
        }else{
            return null;  
        }
    }

    @Override
    public String toStringJdbc() {
        return jdbc.toString();
    }
    
}
