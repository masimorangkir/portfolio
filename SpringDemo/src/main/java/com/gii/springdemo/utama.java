/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gii.springdemo;
import com.gii.model.SourceDto;
import com.gii.service.IMath;
import com.gii.service.ISourceService;
import java.math.BigDecimal;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import com.dashboard.common.fake.IAnimal;
import com.dashboard.common.fake.AnimalImpl;
import com.dashboard.common.util.ReflectionUtil;
import org.springframework.util.ReflectionUtils;


/**
 *
 * @author Genesys8
 */
public class utama {
    public static void main(String[] asdf){
        ApplicationContext ctx = new ClassPathXmlApplicationContext("DemoContext.xml");
        IMath iocMath = ctx.getBean(IMath.class);
       // Object hei= ctx.getBean("dataSource");
       // System.out.println("Data Source ="+hei.toString());
        ISourceService iocSource = ctx.getBean(ISourceService.class);
        System.out.println("JdbcString -- "+iocSource.toStringJdbc());
        SourceDto result =  iocSource.findByName("lj");
        System.out.println(ReflectionUtil.printGetMethodValues(result));
        System.out.println(ReflectionUtil.printPropertyValues(result));
        //berasal dari <import resource=...> di application context
        IAnimal iocAnimal = ctx.getBean(IAnimal.class);
        System.out.println("IAnimal -- "+iocAnimal.printName());
        System.out.println(ReflectionUtil.printGetMethodValues(iocAnimal));
        
        BigDecimal hasil =  iocMath.additionOperation(BigDecimal.valueOf(30.5), BigDecimal.valueOf(20.1)); 
        System.out.println(hasil.toPlainString());
        System.out.println("Masuk yang awal lho");
        System.out.println("--------------------------");
        
        for (String isi : ctx.getBeanDefinitionNames()) {
            System.err.println("Bean :: "+isi);
        }
    }
}
