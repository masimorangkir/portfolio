/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dashboard.common.aop;

import com.dashboard.common.exception.CommonAuthenticationException;
import com.dashboard.common.model.session.UserSession;
import javax.servlet.http.HttpServletRequest;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;

@Aspect
public class AuthenticationAspect {

    @Autowired
    UserSession session;
    
    //required=false  --> Avoid dependency injection error when do unit testing of AuthenticationAspect
    //because HttpServletRequest is only available during running web apps only
    @Autowired(required = false)
    HttpServletRequest req;

    //@Before("execution(* com.dashboard.common.aop.OmongKosong.cetak(..) )")
    /*@Before(value = "@annotation(auth)")
     public void isengAjaKoq(final Authentication auth){
     System.out.println("LOG Before is running");
     }
    
     @Before(value="@within(auth)")
     public void isengClass(final Authentication auth){
     System.out.println("LOG Class level is running");
     }*/

    @Before(value = "@within(auth)")
    public void checkForClassObject(JoinPoint jp, final Authentication auth) throws CommonAuthenticationException {
        String methodName = jp.getSignature().getName();
        System.out.print("Enter AuthenticationAspect " + methodName);
        checkLogin(auth);
    }

    @Before(value = "@annotation(auth)")
    public void checkForMethod(JoinPoint jp, final Authentication auth) throws CommonAuthenticationException {
        String methodName = jp.getSignature().getName();
        System.out.print("Enter AuthenticationAspect " + methodName);
        checkLogin(auth);
    }

    private void checkLogin(final Authentication auth) throws CommonAuthenticationException {
        if (!session.isLogin()) { //if not authenticated user -> throw exception. Checking mechanism will be implemented later
            System.out.println("Throwing CommonAuthenticationException...");
            if (req != null) {
                session.setTraceBackUrl(req.getRequestURL().toString());
            }
            throw new CommonAuthenticationException("Access Denied. You must login");
        }
    }
}
