package com.dashboard.common.callback;

import com.dashboard.common.model.HighchartMap;
import com.dashboard.common.model.HighchartSeries;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowCountCallbackHandler;

/**
 *
 * @author Genesys8
 */
public class HighchartRowCountCallback extends RowCountCallbackHandler {

    private HighchartMap hMap = new HighchartMap();
    private int totalColumn = 0;
    private int mainCounter = 0;

    @Override
    protected void processRow(ResultSet rs, int rowNum) throws SQLException {
        HighchartSeries series = null;
        totalColumn = getColumnCount();
        int index = 0;
        for (String name : getColumnNames()) {
            if (mainCounter == 0) { //only do it on first row only
                if (index > 0) { //ignore first column to add into Categories
                    hMap.getCategories().add(name);
                }
            }
            if (index == 0) {
                String res = rs.getString(name);
                series = new HighchartSeries();
                series.setName(res);
            } else {
                BigDecimal res = rs.getBigDecimal(name);
                series.getData().add(res);
            }
            index++;
        }
        if (series != null) {
            hMap.getSeries().add(series);
        }
        mainCounter++;
    }

    public int getTotalColumn() {
        return this.totalColumn;
    }

    public HighchartMap getHighchartMap() {
        return this.hMap;
    }

}
