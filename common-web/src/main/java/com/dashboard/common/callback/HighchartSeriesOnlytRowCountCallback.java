package com.dashboard.common.callback;

import com.dashboard.common.model.HighchartMap;
import com.dashboard.common.model.HighchartSeries;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.jdbc.core.RowCountCallbackHandler;

//Note :: column index is not zero-based index !!!!
public class HighchartSeriesOnlytRowCountCallback extends RowCountCallbackHandler {

    private HighchartMap hMap = new HighchartMap();
    private int totalColumn = 0;
    private int mainCounter = 0;
    private int indexSeriesData = 0;
    private int indexSeriesName = 0;
    private HighchartSeries series;

    public HighchartSeriesOnlytRowCountCallback(int indexName, int indexData) {
        this.indexSeriesName = indexName;
        this.indexSeriesData = indexData;
        series = new HighchartSeries();
        series.setName("Accumulative");
        hMap.getSeries().add(series);
    }
    
    public void setHorizontalTitle(String val){
        series.setName(val);
    }

    @Override
    protected void processRow(ResultSet rs, int rowNum) throws SQLException {
//        HighchartSeries series = null;
//        series = new HighchartSeries();
//        series.setName( rs.getString(this.indexSeriesName));
        List<Object> data = new ArrayList<>();
        data.add(rs.getString(this.indexSeriesName));
        data.add(rs.getBigDecimal(this.indexSeriesData));
        series.getData().add(data);
//        series.setData(data);
        
//        series.hMap.getCategories().add(rs.getString(this.indexSeriesName));
//        series.getData().add(rs.getBigDecimal(this.indexSeriesData));
//        hMap.getSeries().add(series);
    }

    public HighchartMap getHighchartMap() {
        
        return this.hMap;
    }

}
