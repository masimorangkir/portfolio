/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dashboard.common.controller;

import com.dashboard.common.exception.CommonAuthenticationException;
import com.dashboard.common.model.session.CompanySetting;
import com.dashboard.common.model.session.UserSession;
import com.dashboard.common.service.ICompanySettingService;
import com.dashboard.common.util.ReflectionUtil;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

/**
 *
 * @author Genesys8
 */
public class BaseController {
    @Autowired
    protected HttpServletRequest _request;
    
    @Autowired
    protected HttpSession _session;
    
    @Autowired
    protected UserSession userSession;
    
    @Autowired
    protected CompanySetting companySetting;
    
    @Autowired
    protected ICompanySettingService companyService;
    
    @ModelAttribute("getUserSession")
    protected UserSession getUserSession(){
        return userSession;
    }
    
    @ModelAttribute("getCompanySetting")
    protected CompanySetting getCompanySetting(){
        return companyService.getCompanySetting();
    }
    
    @ModelAttribute("isLogin")
    protected boolean isUserLogin(){
        return StringUtils.isNotBlank(userSession.getName())?true:false;
    }
    protected void createSession(){
        _request.getSession(true);
    }
    protected void destroySession(){
        _session.invalidate();
    }
    
    String errMessage = "";
    @Autowired HttpServletRequest requ;
    @ExceptionHandler(CommonAuthenticationException.class)
    public ModelAndView resolveException(CommonAuthenticationException ex,HttpServletRequest req){
        System.out.println("Masuk ResolveException Common");
        System.out.println("Error ResolveException Common :: "+ex.getMessage());
        System.out.println("TraceBack Session ::" +userSession.getTraceBackUrl());
        return handlingExceptionHandler(ex);
    }
   /* @ExceptionHandler(Exception.class)
    public ModelAndView resolveException2(Exception ex){
        System.out.println("Masuk ResolveException untuk Exception.class");
        System.out.println("Error ResolveException :: "+ex.getMessage());
        return handlingExceptionHandler(ex);
    }*/
    private ModelAndView handlingExceptionHandler(Exception ex){
         errMessage = (StringUtils.isBlank(ex.getMessage()) ? "You don't have access right... Please login" : ex.getMessage());
        if(!userSession.isLogin()){
            return new ModelAndView("/login").addObject("error", errMessage);
        }else
        {
            return new ModelAndView("redirect:"+userSession.getTraceBackUrl());
            //return new ModelAndView("production/x");
        }
    }
    
    protected Object redirectToTraceBackOrLoginPage(){
        System.out.println("CALL redirectToTraceBackOrLoginPage...");
        System.out.println(ReflectionUtil.printGetMethodValues(userSession));
         if(StringUtils.isNotBlank(userSession.getTraceBackUrl())){
            String backUrl = userSession.getTraceBackUrl();
            System.out.println("TraceBack from redirectToTraceBackOrLoginPage="+backUrl);   
            return new RedirectView(backUrl,true,true,false);
        }
         //it means no traceBackUrl
         if(userSession.isLogin()){
             return new RedirectView("/",true,true,false);
         }
         //it means anonymous user
         return new RedirectView("/",true,true,false);
    }


}
