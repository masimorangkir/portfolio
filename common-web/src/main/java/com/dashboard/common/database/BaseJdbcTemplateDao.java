/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dashboard.common.database;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;


/**
 *
 * @author Genesys8
 */
//Note : calling 'new' won't inject the value of jdbcTemplate. 
//How to resolve : 
//1. add @Service or other annotation and get this class through ApplicationContext and always include specific bean's name when calling ApplicationContext.getBean(...)
//2. just @Autowired this bean and never instantiated with 'new' (make sure at least one annotation exist in this bean (e.g in property or etc) to make the automatic instantiation of this bean work)
//@Service(value = "contextJdbcTemplate")
public class BaseJdbcTemplateDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    @Autowired
    private NamedParameterJdbcTemplate namedJdbcTemplate;

    public NamedParameterJdbcTemplate getNamedJdbcTemplate() {
        return namedJdbcTemplate;
    }

    public void setNamedJdbcTemplate(NamedParameterJdbcTemplate namedJdbcTemplate) {
        this.namedJdbcTemplate = namedJdbcTemplate;
    }
    
    protected JdbcTemplate getJdbcTemplate(){
        return this.jdbcTemplate;
    }
    
}
