/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dashboard.common.exception;


/**
 *
 * @author Genesys8
 */
public class CommonAuthenticationException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    public CommonAuthenticationException(String message, Throwable cause){
        super(message, cause);
        
    }
    public CommonAuthenticationException(String message){
      super(message);
      System.out.println("Throwing to parent class");
    }
    
    public CommonAuthenticationException(Throwable thr){
        super(thr);
    }
    public CommonAuthenticationException(){
        super();
    }
    
}
