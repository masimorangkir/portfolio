/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dashboard.common.fake;
import com.dashboard.common.database.BaseJdbcTemplateDao;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
/**
 *
 * @author Genesys8
 */
@Component
public class AnimalImpl extends BaseJdbcTemplateDao implements IAnimal {

    @Override
    public String printName() {
        return "Animal Name from Implementing class";
    }
    
    @Override
    public JdbcTemplate getJdbcTemplate(){
        return super.getJdbcTemplate();
    }
    
}
