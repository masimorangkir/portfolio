/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dashboard.common.fake;

import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author Genesys8
 */
public interface IAnimal {
    public String printName();
    public JdbcTemplate getJdbcTemplate();
}
