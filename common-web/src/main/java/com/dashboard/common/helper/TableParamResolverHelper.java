/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dashboard.common.helper;

import com.dashboard.common.param.ParamDataTables;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Genesys8
 */
public class TableParamResolverHelper {

    static final String PARAM_DRAW = "draw";
    static final String PARAM_START = "start";
    static final String PARAM_LENGTH = "length";
    static final String PARAM_SEARCH = "search[value]";
    static final String PARAM_ORDER_COLUMN = "order[{0}][column]";
    static final String PARAM_ORDER_DIR = "order[{0}][dir]";
    static final String PARAM_PATTERN_ORDER_COLUMN = "order\\[\\d{1,}\\]\\[column\\]";
    static final String PARAM_PATTERN_ORDER_DIR = "order\\[\\d{1,}\\]\\[dir\\]";
    static final String OPEN_TAG = "[";
    static final String CLOSE_TAG = "]";

    public static ParamDataTables resolve(Map<String, String> params) {
        if (params == null) {
            return null;
        }
        ParamDataTables par = new ParamDataTables();
        if (params.containsKey(PARAM_DRAW)) {
            par.setDraw(Integer.parseInt(params.get(PARAM_DRAW)));
        }
        if (params.containsKey(PARAM_START)) {
            par.setStart(Integer.parseInt(params.get(PARAM_START)));
        }
        if (params.containsKey(PARAM_LENGTH)) {
            par.setLength(Integer.parseInt(params.get(PARAM_LENGTH)));
        }
        if (params.containsKey(PARAM_SEARCH)) {
            par.setSearchBy(params.get(PARAM_SEARCH));
        }
        String orderBy = "";

        Map<String, String> mapOrderBy = new HashMap<String, String>();
        for (Map.Entry<String, String> entry : params.entrySet()) {
            String test = entry.getKey();
            String sortColumnIndex = entry.getValue();

            String indexOfParameter = "";
            if (test.matches(PARAM_PATTERN_ORDER_COLUMN)) {
                indexOfParameter = StringUtils.substringBetween(test, "[", "]");
                if (StringUtils.isNotBlank(indexOfParameter)) {
                    mapOrderBy.put(indexOfParameter, sortColumnIndex);
                }
            }
        }

        //let's sort mapOrderBy to preserve the correct order of 'sort by'
        Map<String, String> sortMap = new TreeMap<String, String>(mapOrderBy);

        List<String> lstOrders = new ArrayList<String>();
        for (Map.Entry<String, String> entry : sortMap.entrySet()) {
            String strKey = entry.getKey();
            String holdValue = params.get(StringUtils.replace(PARAM_ORDER_DIR, "{0}", strKey));
            lstOrders.add(entry.getValue() + " " + holdValue);
        }

        if (lstOrders.size() > 0) {
            orderBy = " order by " + StringUtils.join(lstOrders, ",");
            par.setOrderByClause(orderBy);
        }
        return par;
    }

}
