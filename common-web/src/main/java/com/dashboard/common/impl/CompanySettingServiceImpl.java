/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dashboard.common.impl;

import com.dashboard.common.database.BaseJdbcTemplateDao;
import com.dashboard.common.service.ICompanySettingService;
import com.dashboard.common.model.session.CompanySetting;
import java.util.List;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.stereotype.Service;

/**
 *
 * @author Genesys8
 */
@Service
public class CompanySettingServiceImpl extends BaseJdbcTemplateDao implements ICompanySettingService {

    @Override
    public CompanySetting getCompanySetting() {
        List<CompanySetting> result = this.getJdbcTemplate().query("select * from m_company", ParameterizedBeanPropertyRowMapper.newInstance(CompanySetting.class));
        if(result.size()>0){
            return result.get(0);
        }
        return null;
    }
    
}
