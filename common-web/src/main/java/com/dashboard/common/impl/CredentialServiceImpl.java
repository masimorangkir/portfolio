/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dashboard.common.impl;

import com.dashboard.common.database.BaseJdbcTemplateDao;
import com.dashboard.common.exception.CommonAuthenticationException;
import com.dashboard.common.model.Credential;
import com.dashboard.common.model.CredentialRole;
import com.dashboard.common.model.session.UserSession;
import com.dashboard.common.service.ICredentialService;
import com.dashboard.common.util.BCrypt;
import com.dashboard.common.util.ReflectionUtil;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.SerializationUtils;
import org.joda.time.DateTime;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.stereotype.Service;

/**
 *
 * @author Genesys8
 */
@Service
public class CredentialServiceImpl extends BaseJdbcTemplateDao implements ICredentialService {

    @Override
    public Credential GetCredentialInfoByName(String user) {
        List<Credential> lookup = basicCredentialInfoHandler(user);
        if (lookup.size() > 0) {
            return lookup.get(0);
        } else {
            return null;
        }
    }

    private List<Credential> basicCredentialInfoHandler(String user) {
        String raw = "select * from credential where user=:user";
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("user", user);
        List<Credential> lookup = getNamedJdbcTemplate().query(raw, param, ParameterizedBeanPropertyRowMapper.newInstance(Credential.class));
        return lookup;
    }

    @Override
    public Credential getAndValidateCredentialInfo(String user, String password) {
        List<Credential> lookup = basicCredentialInfoHandler(user);
        if (lookup.size() > 0) {
            //validate password
            boolean isValid = BCrypt.checkpw(password, lookup.get(0).getPassword());
            if (!isValid) {
                throw new CommonAuthenticationException("Invalid username or password");
            }
        } else {
            throw new CommonAuthenticationException("Invalid username or password");
        }
        return lookup.get(0);
    }

    @Override
    public CredentialRole[] getCredentialRole(Credential crd) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void blockCredential(Credential executor, Credential target) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void createCredential(Credential creator, Credential target) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateCredential(String executor, Credential target, Credential overridingBean) {
        String userModified = executor;
        //preserve password before object cloning
        String targetPassword = target.getPassword();
        //it won't clone the base's properties (dateModified,dateCreated,etc)
        target = SerializationUtils.clone(overridingBean);
        //write back the original password
        target.setPassword(targetPassword);
        
        System.out.println(ReflectionUtil.printGetMethodValues(target));

        //database update
        //parUser varchar(250),parPassword varchar(250),parName varchar(250),parPhone varchar(250),parMobile varchar(250), parBirthDate varchar(250),parBirthPlace varchar(250),parModifier varchar(250)
        Map<String, Object> mp = new HashMap<>();
        mp.put("user", overridingBean.getUser());
        if (!target.getPassword().equals(overridingBean.getPassword())) {
            mp.put("password", BCrypt.hashpw(overridingBean.getPassword(), BCrypt.gensalt(13)));
        }else{
            mp.put("password", overridingBean.getPassword()); //don't hash the password here !!!
        }
        mp.put("name", overridingBean.getName());
        mp.put("phone", overridingBean.getPhone());
        mp.put("mobile", overridingBean.getMobile());
        mp.put("birthDate", overridingBean.getBirthDate());
        mp.put("birthPlace", overridingBean.getBirthPlace());
        mp.put("email", overridingBean.getEmail());
        mp.put("modifier", userModified);
        getNamedJdbcTemplate().update("call sp_updateCredential(:user,:password,:name,:phone,:mobile,:birthDate,:birthPlace,:email,:modifier)", mp);
    }

    @Override
    public void passCredentialToUserSession(Credential source, UserSession session, String ipAddress) {
        // session.setLogTime ();
        DateTime jDate = new DateTime();
        Date logDate = jDate.toDate();
        session.setLogTime(logDate);
        session.setName(source.getUser());
        session.setSource(source.getSource());
        session.setIpAddress(ipAddress);
        Map<String, Object> mp = new HashMap<>();
        mp.put("paramUser", source.getUser());
        mp.put("paramIP", ipAddress);
        getNamedJdbcTemplate().update("call sp_insertAudit(:paramUser,:paramIP)", mp);
    }

    @Override
    public void copyRoleToUserSession(Credential target, UserSession session) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
