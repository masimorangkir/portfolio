/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dashboard.common.interceptor;

import com.dashboard.common.model.session.UserSession;
import com.dashboard.common.util.SpringApplicationContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Genesys8
 */
public class BaseAuthenticationInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest hsr, HttpServletResponse hsr1, Object o) throws Exception {
        ApplicationContext ctx = SpringApplicationContext.getCurrent();
        System.out.println("Context Path : " + hsr.getContextPath());
        System.out.println("Request URL : " + hsr.getRequestURL());
        System.out.println("IP::"+hsr.getRemoteAddr());
        /*  UserSession uss = ctx.getBean(UserSession.class);
        
        if(!StringUtils.contains(reqUrl, "/bundles/")){
            return true;
        }
        if(!StringUtils.contains(reqUrl, "/assets/")){
            return true;
        }
        
        if (!StringUtils.contains(reqUrl, "/login")) {
            System.out.println("Action : check UserSession");
            if (!uss.isLogin()) {
                System.out.println("Decision : NOT LOGIN, please redirect");
                hsr1.sendRedirect(hsr.getContextPath() + "/login");
                return false;
            } else {
                System.out.println("Decision : LOGIN OK");
            }
        } else {
            if(uss.isLogin()){
                hsr1.sendRedirect(hsr.getContextPath() + "/production");
            }
        }*/
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest hsr, HttpServletResponse hsr1, Object o, ModelAndView mav) throws Exception {
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void afterCompletion(HttpServletRequest hsr, HttpServletResponse hsr1, Object o, Exception excptn) throws Exception {
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
