package com.dashboard.common.model;

import java.util.ArrayList;
import java.util.List;


public class HighchartMap {
    private List<String> categories = new ArrayList<>();
    private List<HighchartSeries> series = new ArrayList<>();

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public List<HighchartSeries> getSeries() {
        return series;
    }

    public void setSeries(List<HighchartSeries> series) {
        this.series = series;
    }
}
