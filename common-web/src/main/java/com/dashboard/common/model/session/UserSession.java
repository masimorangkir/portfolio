/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dashboard.common.model.session;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Genesys8
 */
public class UserSession implements Serializable {

    private String name;
    private Date logTime;
    private String source;
    private String ipAddress;
    private String traceBackUrl;

    public String getTraceBackUrl() {
        return traceBackUrl;
    }

    public void setTraceBackUrl(String traceBackUrl) {
        this.traceBackUrl = traceBackUrl;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Date getLogTime() {
        return logTime;
    }

    public void setLogTime(Date logTime) {
        this.logTime = logTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isLogin() {
        if (StringUtils.isBlank(name)) {
            return false;
        }
        return true;
    }
}
