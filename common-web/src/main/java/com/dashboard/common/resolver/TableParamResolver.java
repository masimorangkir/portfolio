package com.dashboard.common.resolver;

import com.dashboard.common.annotation.TableParam;
import com.dashboard.common.helper.TableParamResolverHelper;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebArgumentResolver;
import org.springframework.web.context.request.NativeWebRequest;

public class TableParamResolver implements WebArgumentResolver {

    @Override
    public Object resolveArgument(MethodParameter mp, NativeWebRequest nwr) throws Exception {
        TableParam par = mp.getParameterAnnotation(TableParam.class);
        if (par != null) {
            HttpServletRequest req = (HttpServletRequest) nwr.getNativeRequest();
            Map<String, String> mapNew = new HashMap<>();
            Enumeration<String> enumString = req.getParameterNames();
            while (enumString.hasMoreElements()) {
                String strKey = enumString.nextElement();
                String[] strVal = req.getParameterValues(strKey);
                if (strVal.length > 0) {
                    mapNew.put(strKey, strVal[0]);
                }
            }
            return TableParamResolverHelper.resolve(mapNew);
        }
        return UNRESOLVED;
    }

}
