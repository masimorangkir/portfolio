/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dashboard.common.service;

import com.dashboard.common.model.session.CompanySetting;

/**
 *
 * @author Genesys8
 */
public interface ICompanySettingService {
    public CompanySetting getCompanySetting();
}
