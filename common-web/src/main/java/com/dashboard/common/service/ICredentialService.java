/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dashboard.common.service;

import com.dashboard.common.model.Credential;
import com.dashboard.common.model.CredentialRole;
import com.dashboard.common.model.session.UserSession;

/**
 *
 * @author Genesys8
 */
public interface ICredentialService {
    public Credential getAndValidateCredentialInfo(String user, String password);
    public Credential GetCredentialInfoByName(String user);
    public CredentialRole[] getCredentialRole(Credential crd);
    public void blockCredential(Credential executor, Credential target);
    public void createCredential(Credential creator, Credential target);
    public void updateCredential(String executor, Credential target, Credential overridingBean);
    public void passCredentialToUserSession(Credential source, UserSession session, String ipAddress);
    public void copyRoleToUserSession(Credential target, UserSession session);
}
