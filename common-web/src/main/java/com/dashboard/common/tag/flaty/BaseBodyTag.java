/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dashboard.common.tag.flaty;

import java.io.IOException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Genesys8
 */
public class BaseBodyTag extends BodyTagSupport {

    protected String caption;
    protected String link;
    protected String icon;
    public String idComponent;
    public boolean skipEvalTag;
    public String columnLayoutClass;

    public String getColumnLayoutClass() {
        return (StringUtils.isBlank(columnLayoutClass)) ? "" : columnLayoutClass;
    }

    public void setColumnLayoutClass(String columnLayoutClass) {
        this.columnLayoutClass = columnLayoutClass;
    }

    public boolean isSkipEvalTag() {
        return skipEvalTag;
    }

    public void setSkipEvalTag(boolean skipEvalTag) {
        this.skipEvalTag = skipEvalTag;
    }

    public String getIdComponent() {
        return (StringUtils.isBlank(idComponent)) ? "" : idComponent;
    }

    public void setIdComponent(String idComponent) {
        this.idComponent = idComponent;
    }

    public String getCaption() {
        return (StringUtils.isBlank(caption)) ? "" : caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getLink() {
        return (StringUtils.isBlank(link)) ? "" : link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getIcon() {
        return (StringUtils.isBlank(icon)) ? "" : icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    protected String OPENING_TAG;
    protected String CLOSING_TAG;
    protected String OPENING_TAG_CONTENT;
    protected String CLOSING_TAG_CONTENT;
    protected String content = "";

    /**
     * To set OPENING_TAG, CLOSING_TAG, OPENING_TAG_CONTENT, CLOSING_TAG_CONTENT
     */
    protected void initializeTagz() throws JspException {
        this.OPENING_TAG = "";
        this.OPENING_TAG_CONTENT = "";
        this.CLOSING_TAG_CONTENT = "";
        this.CLOSING_TAG = "";

    }

    protected String renderContent(String content) {
        return OPENING_TAG_CONTENT + content + CLOSING_TAG_CONTENT;
    }

    protected String replaceAtributz(String source) {
        String[] path = {"@link", "@faIcon", "@caption"};
        String path2 = "@@id";
        String repl2 = " id=\"" + getIdComponent() + "\" ";
        String[] repl = {getLink(), getIcon(), getCaption()};
        source = StringUtils.replaceEach(source, path, repl);
        if (!getIdComponent().equalsIgnoreCase("")) {
            source = StringUtils.replace(source, path2, repl2);
        } else {
            source = StringUtils.replace(source, path2, "");
        }
        return source;
    }

    @Override
    public int doStartTag() throws JspException {
//        getBodyContent() is not applicable here        
//        BodyContent bc = getBodyContent();
//        content = bc.getString();
        try {
            initializeTagz();
            this.pageContext.getOut().print(OPENING_TAG);
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
        if (isSkipEvalTag()) {
            return SKIP_BODY;
        } else {
            return EVAL_BODY_BUFFERED; //default response            
        }
        //return EVAL_BODY_INCLUDE; //will result an error in getBodyContent in doAfterBody()
    }

    @Override
    public int doEndTag() throws JspException {
        try {
            initializeTagz();
            this.pageContext.getOut().print(CLOSING_TAG);
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
        return EVAL_PAGE;
    }

    @Override
    public int doAfterBody() throws JspException {
        try {
            initializeTagz();
            BodyContent bc = getBodyContent();
            content = bc.getString();
            content = renderContent(content);
            bc.getEnclosingWriter().print(content);
        } catch (IOException e) {
            throw new JspException("Error :: " + e.getMessage());
        }
        return SKIP_BODY;
    }
}
