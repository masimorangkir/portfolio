/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dashboard.common.tag.flaty;

import com.dashboard.common.tag.flaty.helper.FlatyBoxActionHelper;
import javax.servlet.jsp.JspException;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Genesys8
 */
public class BoxContentWrapTag extends BaseBodyTag{

    public boolean useSlimScroll;
    public boolean useListStyle;
    public String className;

    public String getClassName() {
        return (StringUtils.isBlank(className))?"":className;
    }
    public void setClassName(String className) {
        this.className = className;
    }
    public boolean isUseListStyle() {
        return useListStyle;
    }

    public void setUseListStyle(boolean useListStyle) {
        this.useListStyle = useListStyle;
    }
    
    public boolean isUseSlimScroll() {
        return useSlimScroll;
    }

    public void setUseSlimScroll(boolean useSlimScroll) {
        this.useSlimScroll = useSlimScroll;
    }
    

    @Override
    protected void initializeTagz() throws JspException {
        super.initializeTagz();
        this.OPENING_TAG="<div class=\"box-content\">";
        if(isUseSlimScroll()){
            this.OPENING_TAG+="<div class=\"slimScroll\">";
        }
        if(isUseListStyle()){
            this.OPENING_TAG+="<ul class='@class'>";
            this.CLOSING_TAG+="</ul>";
        }else{
            this.OPENING_TAG+="<div class='@class'>";
            this.CLOSING_TAG+="</div>";
        }
        if(isUseSlimScroll()){
            this.CLOSING_TAG+="</div>";
        }
        this.CLOSING_TAG+="</div>";
        this.OPENING_TAG=StringUtils.replace(this.OPENING_TAG, "@class", getClassName());
    }
}
