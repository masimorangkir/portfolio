/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dashboard.common.tag.flaty;

import com.dashboard.common.tag.flaty.helper.FlatyBoxActionHelper;
import javax.servlet.jsp.JspException;

/**
 *
 * @author Genesys8
 */
public class BoxTitleAndToolTag extends BaseBodyTag {

    public boolean useCollapse;
    public boolean useClose;

    public boolean isUseCollapse() {
        return useCollapse;
    }

    public void setUseCollapse(boolean useCollapse) {
        this.useCollapse = useCollapse;
    }

    public boolean isUseClose() {
        return useClose;
    }

    public void setUseClose(boolean useClose) {
        this.useClose = useClose;
    }
    
    
    
    @Override
    protected void initializeTagz() throws JspException {
       this.OPENING_TAG="<div class=\"box-title\">"
                        + " <h3><i class=\"fa @faIcon\"></i> @caption</h3>"
                        + " <div class=\"box-tool\">";
       this.OPENING_TAG_CONTENT="";
       this.CLOSING_TAG_CONTENT="";
       this.CLOSING_TAG="";
       if(isUseCollapse()){
           this.CLOSING_TAG+=FlatyBoxActionHelper.renderAction("collapse", "#", "fa-chevron-up");
       }
       if(isUseClose()){
           this.CLOSING_TAG+=FlatyBoxActionHelper.renderAction("close", "#", "fa-times");
       }
       this.CLOSING_TAG+="</div>\n</div>\n";
       this.OPENING_TAG=replaceAtributz(this.OPENING_TAG);
    }
    
}
