/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dashboard.common.tag.flaty;

import com.dashboard.common.tag.flaty.helper.FlatyBoxActionHelper;
import javax.servlet.jsp.JspException;

/**
 *
 * @author Genesys8
 */
public class BoxToolActionNoContentTag extends BaseBodyTag{

    public String action;
    public String href;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    @Override
    protected void initializeTagz() throws JspException {
        super.initializeTagz();
        super.setSkipEvalTag(true); //because this tag require no content to process
        this.OPENING_TAG = FlatyBoxActionHelper.renderAction(action, href, icon);
    }
}
