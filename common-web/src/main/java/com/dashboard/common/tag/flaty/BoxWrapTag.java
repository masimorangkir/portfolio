/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dashboard.common.tag.flaty;

import javax.servlet.jsp.JspException;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Genesys8
 */
public class BoxWrapTag extends BaseBodyTag{
    public String boxColor;

    public String getBoxColor() {
        return boxColor;
    }
    public void setBoxColor(String boxColor) {
        this.boxColor = boxColor;
    }
 
    @Override
    protected void initializeTagz() throws JspException {
        super.initializeTagz();
        boolean colLayout = StringUtils.isNotBlank(getColumnLayoutClass());
        if(colLayout){
            this.OPENING_TAG+="<div class='"+this.getColumnLayoutClass()+"'>";
        }
        this.OPENING_TAG += "<div class=\"box @boxColor\">\n";
        this.CLOSING_TAG += "</div>";
        if(colLayout){
            this.CLOSING_TAG+="</div>";
        }
        String match = "@boxColor";
        String repl = (StringUtils.isBlank(getBoxColor()))?"":"box-"+getBoxColor();
        this.OPENING_TAG = StringUtils.replace(this.OPENING_TAG, match, repl);
    }
    
}
