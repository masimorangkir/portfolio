/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dashboard.common.tag.flaty;

import java.io.IOException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyContent;

/**
 *
 * @author Genesys8
 */
public class DummyTag extends BaseBodyTag {
    private String content="";

    @Override
    protected String renderContent(String content) {
        return super.renderContent(content);
    }

    @Override
    protected void initializeTagz() throws JspException {
        this.OPENING_TAG = "<div class=\"tile tile-orange\">";
        this.CLOSING_TAG = "</div>";
        this.OPENING_TAG_CONTENT = "<a href=#>";
        this.CLOSING_TAG_CONTENT = "</a>";
    }
    
}
