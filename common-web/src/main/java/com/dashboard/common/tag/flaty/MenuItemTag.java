/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dashboard.common.tag.flaty;

import javax.servlet.jsp.JspException;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Genesys8
 */
public class MenuItemTag extends BaseBodyTag {

    public String target;

    public String getTarget() {
        return (StringUtils.isBlank(target)) ? "" : target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    @Override
    protected void initializeTagz() throws JspException {
        this.OPENING_TAG = "<li @@id>\n";
        this.OPENING_TAG_CONTENT = " <a href=\"@link\" @target>\n"
                + "     <i class=\"fa @faIcon\"></i>\n"
                + "         <span>\n";
        this.CLOSING_TAG_CONTENT = "</span>\n</a>";
        this.CLOSING_TAG = "</li>";
        this.OPENING_TAG_CONTENT = replaceAtributz(this.OPENING_TAG_CONTENT);
        this.OPENING_TAG = replaceAtributz(this.OPENING_TAG);

        String _target = "";
        if (StringUtils.isNotBlank(getTarget())) {
            _target = "target='" + getTarget() + "'";
        }
        this.OPENING_TAG_CONTENT = StringUtils.replace(this.OPENING_TAG_CONTENT, "@target", _target);
    }
}
