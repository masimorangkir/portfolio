/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dashboard.common.tag.flaty;

import java.io.IOException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;

/**
 *
 * @author Genesys8
 */
public class PanelTag extends BodyTagSupport {

    @Override
    public int doAfterBody() throws JspException{
        BodyContent bc = getBodyContent();
        String content = bc.getString();
        JspWriter out = bc.getEnclosingWriter();
        try {
            out.print("<div class='panel'>\n"
                        +content+"\n"
                    +"\n</div>");   
        } catch (IOException e) {
            throw new JspException("Error :: "+e.getMessage());
        }
        return SKIP_BODY;
    }
    
}
