/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dashboard.common.tag.flaty;

import javax.servlet.jsp.JspException;

/**
 *
 * @author Genesys8
 */
public class SlimScrollTag extends BaseBodyTag{

    @Override
    protected void initializeTagz() throws JspException {
        this.OPENING_TAG="<div class=\"slimScroll\">";
        this.OPENING_TAG_CONTENT="";
        this.CLOSING_TAG_CONTENT="";
        this.CLOSING_TAG="</div>";
    }
    
}
