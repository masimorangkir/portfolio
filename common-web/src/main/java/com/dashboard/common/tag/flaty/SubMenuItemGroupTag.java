/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dashboard.common.tag.flaty;

import javax.servlet.jsp.JspException;

/**
 *
 * @author Genesys8
 */
public class SubMenuItemGroupTag extends BaseBodyTag {
    @Override
    protected void initializeTagz() throws JspException {
        this.OPENING_TAG="<li>\n"
                        +" <span>@caption</span>\n" +
"                                <ul>";
        this.OPENING_TAG_CONTENT="";
        this.CLOSING_TAG_CONTENT="";
        this.CLOSING_TAG="</ul>\n"
                         +"</li>";
        this.OPENING_TAG=replaceAtributz(this.OPENING_TAG);
    }
}
