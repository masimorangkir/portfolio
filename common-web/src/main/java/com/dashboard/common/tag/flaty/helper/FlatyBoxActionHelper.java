/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dashboard.common.tag.flaty.helper;

import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Genesys8
 */
public class FlatyBoxActionHelper {
    public static String renderActionCollapse(){
        String tmpl = "<a data-action=\"collapse\">";
        return "";
    }
    public static String renderAction(String action, String href, String icon){
        if(StringUtils.isBlank(action)){
            action="";
        }
        if(StringUtils.isBlank(href)){
            href="#";
        }
        if(StringUtils.isBlank(icon)){
            icon="";
        }
        String tmpl = "<a data-action=\""+action+"\" href=\""+href+"\"><i class=\"fa "+icon+"\"></i></a>";
        return tmpl;
    }
}
