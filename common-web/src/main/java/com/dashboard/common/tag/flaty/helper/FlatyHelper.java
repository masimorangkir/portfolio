/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dashboard.common.tag.flaty.helper;

import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Genesys8
 */
public class FlatyHelper {
    public static String flatyMenuItem(String caption, String link,String faIcon){
        String template = "<li>\n"
                        + " <a href=\"@link\">\n" +
                          "     <i class=\"fa @faIcon\"></i>\n" +
                          "     <span>@caption</span>\n" +
                          " </a>"
                        + "</li>";
        template=StringUtils.replace(template, "@link", link);
        template=StringUtils.replace(template, "@faIcon", faIcon);
        template=StringUtils.replace(template, "@caption", caption);
        return template;
        
    }
}
