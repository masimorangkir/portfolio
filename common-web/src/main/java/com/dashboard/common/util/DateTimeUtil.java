package com.dashboard.common.util;

import java.util.Date;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class DateTimeUtil {

    public static String DATE_FORMAT_IND = "yyyy-MM-dd";

    public static String validateDateString(String dates) {
        DateTimeFormatter frm = DateTimeFormat.forPattern(DATE_FORMAT_IND);
        String dt = "";
        try {
            frm.parseDateTime(dates);
            dt = dates;
        } catch (Exception e) {
        }
        return dt;
    }
    public static String validateDateString(String dates, String formatDate) {
        DateTimeFormatter frm = DateTimeFormat.forPattern(formatDate);
        String dt = "";
        try {
            frm.parseDateTime(dates);
            dt = dates;
        } catch (Exception e) {
        }
        return dt;
    }
    
    public static Date convertStringToDate(String strDate, String formatDate) throws Exception{
        String value = validateDateString(strDate,formatDate);
        if(value.equalsIgnoreCase("")){
            throw new Exception("Date format is not valid");
        }
        DateTimeFormatter df = DateTimeFormat.forPattern(formatDate);
        DateTime x  = df.parseDateTime(strDate);
        return x.toDate();
    }
}
