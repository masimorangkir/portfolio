/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dashboard.common.database;

import com.dashboard.common.fake.HewanInterface;
import com.dashboard.common.fake.IAnimal;
import com.dashboard.common.util.SpringApplicationContext;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author Genesys8
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:com.dashboard.common.test.xml")
public class BaseJdbctTemplateDaoTest {
    
//    @Autowired //@Qualifier("contextJdbcTemplate")
//    BaseJdbctTemplateDao bean;
//    
//    @Autowired ApplicationContext context;
    @Autowired private IAnimal ianim;
//    @Autowired private JdbcTemplate iTemplate;
//    
    @Before
    public void setup(){
         //context = new ClassPathXmlApplicationContext("classpath:com.dashboard.common.xml");
     //    bean = (BaseJdbctTemplateDao) context.getBean("contextJdbcTemplate",BaseJdbctTemplateDao.class);
    }
//  
//    @Test
//    public void testGetNamedJdbcTemplate(){
//        System.out.println("getNamedJdbcTemplate Test");
//        assertNotNull( bean.getNamedJdbcTemplate());
//        Map<String,Object> map = new HashMap<String, Object>();
//        map.put("param", "KL");
//        int hasilQuery = bean.getNamedJdbcTemplate().queryForInt("select count(*) from source where id = :param", map);
//        System.out.println("Hasil query :: "+hasilQuery);
//    }
//    
//    @Test
//    public void testGetJdbcTemplate() {
//        System.out.println("getJdbcTemplate");
//        System.out.println(ianim.toString());
//        System.out.println(iTemplate.toString());
//        for (String  object :  context.getBeanDefinitionNames()) {
//            System.out.println("Bean Definition :: "+object);
//        }
//        assertNotNull(bean.getJdbcTemplate());      
//        int hasilQuery = bean.getJdbcTemplate().queryForInt("select count(*) from source  where id= ?","KL" );
//        System.out.println("Hasil query :: "+hasilQuery);        
//    }
    
    @Test
    public void testGetJdbcTemplate_AnimalTest(){
       JdbcTemplate template =  ianim.getJdbcTemplate();
       assertNotNull(template);
       System.out.println("From Animal :: "+ template.toString());
    }
 
    @Autowired
    HewanInterface hi;
    @Test
    public void testGetJdbcTemplate_HewanTest(){
        System.out.println("Tolong CETAK :: " + hi.printTemplate());
    }
    
}
