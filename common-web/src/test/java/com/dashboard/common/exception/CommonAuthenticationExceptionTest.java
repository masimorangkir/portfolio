/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dashboard.common.exception;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Genesys8
 */
public class CommonAuthenticationExceptionTest {
    
    public CommonAuthenticationExceptionTest() {
    }

    @Test
    public void testThrowCommonAuthenticatedException() {
        try {
            
         throw new CommonAuthenticationException("Access Denied. You must login");
        } catch (Exception e) {
            System.out.println(e.getClass().toString());
        }
    }
    
}
