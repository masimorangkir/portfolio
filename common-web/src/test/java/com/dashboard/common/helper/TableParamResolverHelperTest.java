/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dashboard.common.helper;

import com.dashboard.common.param.ParamDataTables;
import com.dashboard.common.util.ReflectionUtil;
import java.util.HashMap;
import java.util.Map;
import org.junit.Test;

/**
 *
 * @author Genesys8
 */
public class TableParamResolverHelperTest {

    public TableParamResolverHelperTest() {
    }

    /**
     * Test of resolve method, of class TableParamResolverHelper.
     */
    @Test
    public void testFoo() {
        System.out.println("foo Test");
        Map<String, String> request = new HashMap<String, String>();
        request.put("draw", "7");
        request.put("columns[0][data]", "0");
        request.put("columns[0][name]", "");
        request.put("columns[0][searchable]", "TRUE");
        request.put("columns[0][orderable]", "TRUE");
        request.put("columns[0][search][value]", "");
        request.put("columns[0][search][regex]", "FALSE");
        request.put("columns[1][data]", "1");
        request.put("columns[1][name]", "");
        request.put("columns[1][searchable]", "TRUE");
        request.put("columns[1][orderable]", "TRUE");
        request.put("columns[1][search][value]", "");
        request.put("columns[1][search][regex]", "FALSE");
        request.put("columns[2][data]", "2");
        request.put("columns[2][name]", "");
        request.put("columns[2][searchable]", "TRUE");
        request.put("columns[2][orderable]", "TRUE");
        request.put("columns[2][search][value]", "");
        request.put("columns[2][search][regex]", "FALSE");
        request.put("columns[3][data]", "3");
        request.put("columns[3][name]", "");
        request.put("columns[3][searchable]", "TRUE");
        request.put("columns[3][orderable]", "TRUE");
        request.put("columns[3][search][value]", "");
        request.put("columns[3][search][regex]", "FALSE");
        request.put("columns[4][data]", "4");
        request.put("columns[4][name]", "");
        request.put("columns[4][searchable]", "TRUE");
        request.put("columns[4][orderable]", "TRUE");
        request.put("columns[4][search][value]", "");
        request.put("columns[4][search][regex]", "FALSE");
        request.put("columns[5][data]", "5");
        request.put("columns[5][name]", "");
        request.put("columns[5][searchable]", "TRUE");
        request.put("columns[5][orderable]", "TRUE");
        request.put("columns[5][search][value]", "");
        request.put("columns[5][search][regex]", "FALSE");
        request.put("order[0][column]", "2");
        request.put("order[0][dir]", "asc");
        request.put("order[1][column]", "1");
        request.put("order[1][dir]", "asc");
        request.put("order[2][column]", "18");
        request.put("order[2][dir]", "desc");
        request.put("start", "0");
        request.put("length", "10");
        request.put("search[value]", "Account");
        request.put("search[regex]", "FALSE");

        ParamDataTables expResult = null;
        ParamDataTables result = TableParamResolverHelper.resolve(request);
        
        System.out.println(ReflectionUtil.printPropertyValues(result));
    }

}
