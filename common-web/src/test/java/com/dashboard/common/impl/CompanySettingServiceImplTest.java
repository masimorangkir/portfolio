/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dashboard.common.impl;

import com.dashboard.common.aop.OmongKosong;
import com.dashboard.common.model.session.CompanySetting;
import com.dashboard.common.service.ICompanySettingService;
import com.dashboard.common.util.ReflectionUtil;
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author Genesys8
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:com.dashboard.common.test.xml")
public class CompanySettingServiceImplTest {
    
  @Autowired private ICompanySettingService companySrv;
    
   
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getCompanySetting method, of class CompanySettingServiceImpl.
     */
    @Test
    public void testGetCompanySetting() {
        System.out.println("getCompanySetting");
        CompanySetting setting =  companySrv.getCompanySetting();
        assertNotNull(setting);
        if(setting!=null) System.out.println(ReflectionUtil.printGetMethodValues(setting));
    }
    
    @Autowired
    ApplicationContext ctx;
    @Test
    public void test(){
      //  ctx = new ClassPathXmlApplicationContext("classpath:com.dashboard.common.xml");
        
    //  OmongKosong ok = (OmongKosong)  ctx.getBean(OmongKosong.class);
        //OmongKosong ok = new OmongKosong();
      //  ok.cetak();
    }
    
}
