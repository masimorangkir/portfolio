/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dashboard.common.impl;

import com.dashboard.common.aop.AuthenticationAspect;
import com.dashboard.common.exception.CommonAuthenticationException;
import com.dashboard.common.model.Credential;
import com.dashboard.common.model.CredentialRole;
import com.dashboard.common.model.session.UserSession;
import com.dashboard.common.service.ICredentialService;
import com.dashboard.common.util.ReflectionUtil;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:com.dashboard.common.test.xml")
public class CredentialServiceImplTest {
//

    @Autowired
    ApplicationContext ctx;
    @Autowired
    ICredentialService iService;
//
//    /**
//     * Test of getAndValidateCredentialInfo method, of class
//     * CredentialServiceImpl.
//     */
//    @Before
//    public void initial() {
//
//    }
//

    @Test
    public void getPasswordTest() {
        Credential creden = iService.GetCredentialInfoByName("marty");
        if (creden != null) {
            System.out.println("Credential ::\n" + ReflectionUtil.printGetMethodValues(creden));
        }
    }
//    @Test
//    public void testGetAndValidateCredentialInfo() {
//        System.out.println("getAndValidateCredentialInfo");
//        String user = "marty";
//        String password = "password123"; 
//        ICredentialService instance = ctx.getBean(ICredentialService.class);
//        Credential result = instance.getAndValidateCredentialInfo(user, password);
//        System.out.println(ReflectionUtil.printGetMethodValues(result));
//        assertNotNull(result);
//    }
//
//    @Test
//    public void testGetCredentialInfoByName() {
//        System.out.println("getAndValidateCredentialInfo");
//        String user = "marty";
//        String password = "password123";
//        ICredentialService instance = ctx.getBean(ICredentialService.class);
//        Credential result = instance.GetCredentialInfoByName(user);
//        System.out.println(ReflectionUtil.printGetMethodValues(result));
//        assertNotNull(result);
//    }
//
//    @Test(expected = CommonAuthenticationException.class)
//    public void testGetAndValidateCredentialInfo_failed() {
//        System.out.println("getAndValidateCredentialInfo");
//        String user = "martinusAAA";
//        String password = "password123";
//        ICredentialService instance = ctx.getBean(ICredentialService.class);
//        Credential result = instance.getAndValidateCredentialInfo(user, password);
//        assertNotNull(result);
//    }
//
//    @Test
//    public void testPassCredentialToUserSession() {
//
//        Credential c = new Credential();
//        String iName = "Jokko";
//        String iSource = "LA";
//        c.setUser(iName);
//        c.setSource(iSource);
//        UserSession sess = new UserSession();
//        iService.passCredentialToUserSession(c, sess, "777.777.888.999");
//        assertEquals(iName, sess.getName());
//        assertEquals(iSource, sess.getSource());
//        String ref = ReflectionUtil.printGetMethodValues(sess);
//        System.out.println(ref);
//    }
//
////    /**
////     * Test of getCredentialRole method, of class CredentialServiceImpl.
////     */
////   @Test
////    public void testGetCredentialRole() {
////        System.out.println("getCredentialRole");
////        Credential crd = null;
////        CredentialServiceImpl instance = new CredentialServiceImpl();
////        CredentialRole[] expResult = null;
////        CredentialRole[] result = instance.getCredentialRole(crd);
////        assertArrayEquals(expResult, result);
////        // TODO review the generated test code and remove the default call to fail.
////        fail("The test case is a prototype.");
////    }
////
////    /**
////     * Test of blockCredential method, of class CredentialServiceImpl.
////     */
////    @Test
////    public void testBlockCredential() {
////        System.out.println("blockCredential");
////        Credential target = null;
////        Credential target = null;
////        CredentialServiceImpl instance = new CredentialServiceImpl();
////        instance.blockCredential(target, target);
////        // TODO review the generated test code and remove the default call to fail.
////        fail("The test case is a prototype.");
////    }
////
////    /**
////     * Test of createCredential method, of class CredentialServiceImpl.
////     */
////    @Test
////    public void testCreateCredential() {
////        System.out.println("createCredential");
////        Credential creator = null;
////        Credential target = null;
////        CredentialServiceImpl instance = new CredentialServiceImpl();
////        instance.createCredential(creator, target);
////        // TODO review the generated test code and remove the default call to fail.
////        fail("The test case is a prototype.");
////    }
////
////    /**
////     * Test of updateCredential method, of class CredentialServiceImpl.
////     */
//    @Test
//    public void testUpdateCredential_notChangePassword() {
//        System.out.println("updateCredential");
//        Credential target = iService.GetCredentialInfoByName("marty");
//        Credential overridingBean = iService.GetCredentialInfoByName("marty");
//        overridingBean.setName("Marty Khan");
//        overridingBean.setBirthPlace("Originally from Heaven");
//        overridingBean.setMobile("HP NOKIA 0300-5595");
//        overridingBean.setEmail("martinus@yahoo.com");
//        // overridingBean.setPassword("password123");
//        System.out.println(ReflectionUtil.printGetMethodValues(target));
//        iService.updateCredential("soro aku", target, overridingBean);
//        
//        //get data from database once more time
//        Credential testing = iService.GetCredentialInfoByName("marty");
//        assertEquals(target.getPassword(), testing.getPassword());
//    }
//
//    @Test
//    public void testUpdateCredential_changePassword() {
//        System.out.println("updateCredential");
//        String modifiedPassword = "password123";
//        Credential executor = iService.GetCredentialInfoByName("marty");
//        Credential overridingBean = iService.GetCredentialInfoByName("marty");
//        overridingBean.setName("Marty Khan");
//        overridingBean.setBirthPlace("Originally from Heaven");
//        overridingBean.setMobile("HP NOKIA 0300-5595");
//        overridingBean.setPassword(modifiedPassword);
//        overridingBean.setEmail("goo@l.com");
//        System.out.println(ReflectionUtil.printGetMethodValues(executor));
//        iService.updateCredential("soro aku", executor, overridingBean);
//        
//        //validate updated password
//        Credential cd=iService.getAndValidateCredentialInfo("marty", modifiedPassword);
//        assertNotNull(cd);
//
//    }
////
////    /**
////     * Test of passCredentialToUserSession method, of class CredentialServiceImpl.
////     */
////    @Test
////    public void testCopyCredentialToUserSession() {
////        System.out.println("passCredentialToUserSession");
////        Credential source = null;
////        UserSession session = null;
////        CredentialServiceImpl instance = new CredentialServiceImpl();
////        instance.passCredentialToUserSession(source, session);
////        // TODO review the generated test code and remove the default call to fail.
////        fail("The test case is a prototype.");
////    }
////
////    /**
////     * Test of copyRoleToUserSession method, of class CredentialServiceImpl.
////     */
////    @Test
////    public void testCopyRoleToUserSession() {
////        System.out.println("copyRoleToUserSession");
////        Credential target = null;
////        UserSession session = null;
////        CredentialServiceImpl instance = new CredentialServiceImpl();
////        instance.copyRoleToUserSession(target, session);
////        // TODO review the generated test code and remove the default call to fail.
////        fail("The test case is a prototype.");
////    }
////
////    
}
