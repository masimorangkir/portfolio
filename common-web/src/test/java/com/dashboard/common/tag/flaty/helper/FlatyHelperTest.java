/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dashboard.common.tag.flaty.helper;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Genesys8
 */
public class FlatyHelperTest {
    
    public FlatyHelperTest() {
    }

    /**
     * Test of flatyMenuItem method, of class FlatyHelper.
     */
    @Test
    public void testFlatyMenuItem() {
        System.out.println("flatyMenuItem");
        String caption = "Gallery";
        String link = "/dashboard/gallery";
        String faIcon = "fa-picture-o";
        caption = "";
        link = "";
        faIcon = null;
        String result = FlatyHelper.flatyMenuItem(caption, link, faIcon);
        System.out.println(result);
    }
    
    @Test
    public void replaceEachTest(){
        String template = "@a @b @c";
        String change[] = {"@a","@b","@c"};
        String repl[]= {"nama","saya","budi"};
        System.out.println(StringUtils.replaceEach(template, change, repl));
    }
    
}
