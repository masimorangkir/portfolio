/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dashboard.common.util;

import java.security.SecureRandom;
import java.util.Date;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.util.StopWatch;

/**
 *
 * @author Genesys8
 */
public class BCryptTest {

    public BCryptTest() {
    }

    /**
     * Test of hashpw method, of class BCrypt.
     */
    @Test
    public void testHashpw() {
        String originalPassword = "password123";
        StopWatch swS  = new StopWatch();
        swS.start();
        String generatedSecuredPasswordHash = BCrypt.hashpw(originalPassword, BCrypt.gensalt(13));
        swS.stop();
        System.out.println(swS.getTotalTimeSeconds());
        System.out.println(generatedSecuredPasswordHash);
        System.out.println("Length = "+ generatedSecuredPasswordHash.length() );
        StopWatch sw = new StopWatch();
        sw.start();
        boolean matched = BCrypt.checkpw(originalPassword, generatedSecuredPasswordHash);
        sw.stop();
        System.out.println(matched);
        System.out.println(sw.getTotalTimeSeconds());
    }
    
    @Test
    public void hapusIni(){
        String kalimat = "order[300][column]";
        String pattern = "order\\[\\d{1,}\\]\\[column\\]";
        System.out.println(pattern);
        System.out.println(kalimat.matches(pattern));
    }
    
    @Test 
    public void testInheritance(){
        Animal x = new Dog();
        x.sound();
    }
    
    @Test
    public void joda(){
        DateTimeFormatter df = DateTimeFormat.forPattern("dd-MM-yyyy");
        DateTime x  = df.parseDateTime("03-12-1988");
        
        System.out.println(x.toString("dd-MM-yyyy"));
    }
    
}

class Animal{
    public void sound(){
        System.out.println("Funny");
    }
}
class Dog extends Animal{
    @Override
    public void sound(){
        System.out.println("So Barky");
    }
}
