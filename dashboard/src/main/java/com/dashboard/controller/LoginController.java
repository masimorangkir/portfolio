/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dashboard.controller;

import com.dashboard.common.controller.BaseController;
import com.dashboard.common.model.Credential;
import com.dashboard.common.service.ICredentialService;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

/**
 *
 * @author Genesys8
 */
@Controller
public class LoginController extends BaseController {

    @Autowired
    ICredentialService iService;
    @Autowired
    HttpServletRequest req;

    @RequestMapping(value = { "/"}, method = RequestMethod.GET)
    public Object goToLogin() {
        if (userSession.isLogin()) {
            return new ModelAndView("show");
        } else {
            return new ModelAndView("login");
        }
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public Object validate(Credential cr, HttpServletRequest req) {
        if (StringUtils.isNotBlank(cr.getUser()) && StringUtils.isNotBlank(cr.getPassword())) {
            if (!userSession.isLogin()) { //if user already logged-in, don't ovewrite existing session
                Credential test = iService.getAndValidateCredentialInfo(cr.getUser(), cr.getPassword());
                super.createSession();;
                iService.passCredentialToUserSession(test, userSession, req.getRemoteAddr());
            }
            return super.redirectToTraceBackOrLoginPage();
        }
        return new RedirectView("/",true,true,false);
    }

    @RequestMapping(value = "/logout")
    public Object logOut() {
        if (userSession.isLogin()) {
            super.destroySession();
        }
        return new RedirectView("/",true,true,false);
    }

}
