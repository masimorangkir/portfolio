package com.dashboard.controller;

import com.dashboard.common.annotation.TableParam;
import com.dashboard.common.aop.Authentication;
import com.dashboard.common.controller.BaseController;
import com.dashboard.common.model.ResponseDataTables;
import com.dashboard.common.param.ParamDataTables;
import com.dashboard.common.util.DateTimeUtil;
import com.dashboard.common.util.ReflectionUtil;
import com.dashboard.model.ReportPIInOut;
import com.dashboard.model.ReportPIInOutWrapper;
import com.dashboard.model.Search2;
import com.dashboard.param.ParamReportPIInOut;
import com.dashboard.service.IPIAjaxFetchService;
import com.dashboard.service.IReportingPIService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Authentication
@Controller
@RequestMapping("/production")
public class ProductionController extends BaseController {

    @Autowired
    IReportingPIService iReportPI;
    @Autowired
    IPIAjaxFetchService ajaxService;

    @RequestMapping(value = {"", "/"})
    public String goToDashboard() {
        return "production";
    }

    @RequestMapping("/getGlobal")
    @ResponseBody
    public Object getGlobalReport_DataTable(@TableParam ParamDataTables par, ParamReportPIInOut parReport) {
        //mocking object   
        String strStart = DateTimeUtil.validateDateString(parReport.getParamStart());
        String strEnd = DateTimeUtil.validateDateString(parReport.getParamEnd());
        strStart = StringUtils.defaultIfBlank(strStart, "2000-01-01");
        strEnd = StringUtils.defaultIfBlank(strEnd, new DateTime().toString("yyyy-MM-dd"));

        parReport.setParamStart(strStart);
        parReport.setParamEnd(strEnd);
        parReport.setParamSearch(par.getSearchBy());
        parReport.setParamPageStart(par.getStart());
        parReport.setParamPageLength(par.getLength());

        System.out.println(ReflectionUtil.printGetMethodValues(parReport));
        System.out.println(ReflectionUtil.printGetMethodValues(par));
        ReportPIInOutWrapper wrapper = iReportPI.getReportGlobal(parReport);
        List<ReportPIInOut> lstIO = wrapper.getListReports();
        ResponseDataTables resp = new ResponseDataTables();
        resp.setDraw(par.getDraw());
        resp.setRecordsTotal(wrapper.getTotalRow());
        resp.setRecordsFiltered(wrapper.getTotalRow());
        for (ReportPIInOut iox : lstIO) {
            resp.getData().add(iox);
        }
        return resp;
    }

    @RequestMapping("/getAccQty")
    @ResponseBody
    public Object getChartAccumulativeQty(@RequestParam(value = "tipe") String reportKind,
            @RequestParam(defaultValue = "", required = false) String uom, @RequestParam(defaultValue = "m", required = false) String period,
            @RequestParam(defaultValue="",required=false) String startDate, @RequestParam(defaultValue = "", required = false) String endDate) {
        return iReportPI.getChartQtyInOut(reportKind, uom, period, startDate, endDate);
    }

    //Note: Return data for chart + table
    @RequestMapping("/getQtyTopTen")
    @ResponseBody
    public Object getTopTenQty(@RequestParam(value = "tipe") String reportKind, String startDate, String endDate) {
        Map<String, Object> mp = new HashMap<>();
        mp.put("chart", iReportPI.getChartQtyTopTenByInventoryId(reportKind, startDate, endDate));
        return mp;
    }
    
    @RequestMapping("/getListQtyTopTen")
    @ResponseBody
    public Object getListTopTenQty(@RequestParam(value = "tipe") String reportKind, String startDate, String endDate){
        Map<String, Object> mp = new HashMap<>();
        mp.put("list", iReportPI.getListQtyTopTenByInventoryName(reportKind, startDate, endDate));
        return mp;
    }

    @RequestMapping("/getPriceTopTen")
    @ResponseBody
    public Object getTopTenPrice(@RequestParam(value = "tipe") String reportKind, String startDate, String endDate) {
        Map<String, Object> mp = new HashMap<>();
        mp.put("chart", iReportPI.getChartPriceTopTenByInventoryId(reportKind, startDate, endDate));
        return mp;
    }

    @RequestMapping("/getListPriceTopTen")
    @ResponseBody
    public Object getListTopTenPrice(@RequestParam(value = "tipe") String reportKind, String startDate, String endDate) {
        Map<String, Object> mp = new HashMap<>();
        mp.put("list", iReportPI.getListPriceTopTenByInventoryName(reportKind, startDate, endDate));
        return mp;
    }
    
    @RequestMapping("/search/customer")
    @ResponseBody
    public Object getSOCustomerByAjax(@RequestParam(defaultValue = "%", required = false) String key) {
        //returning select2 format
        List<String> original = ajaxService.getAllSOCustomerName(key);
        List<Search2> wrapper = new ArrayList<>();
        wrapper.add(new Search2(key));
        for (String data : original) {
            wrapper.add(new Search2(data));
        }
        return wrapper;
    }

    @RequestMapping("/search/pi")
    @ResponseBody
    public Object getPIByAjax(@RequestParam(defaultValue = "%", required = false) String key) {
        List<String> original = ajaxService.getAllPIID(key);
        List<Search2> wrapper = new ArrayList<>();
        wrapper.add(new Search2(key));
        for (String data : original) {
            wrapper.add(new Search2(data));
        }
        return wrapper;
    }

    @RequestMapping("/search/so")
    @ResponseBody
    public Object getSOByAjax(@RequestParam(defaultValue = "%", required = false) String key) {
        List<String> original = ajaxService.getAllSOID(key);
        List<Search2> wrapper = new ArrayList<>();
        wrapper.add(new Search2(key));
        for (String data : original) {
            wrapper.add(new Search2(data));
        }
        return wrapper;
    }
}
