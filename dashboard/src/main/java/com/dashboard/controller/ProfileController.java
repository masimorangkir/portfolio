/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dashboard.controller;

import com.dashboard.common.aop.Authentication;
import com.dashboard.common.controller.BaseController;
import com.dashboard.common.model.Credential;
import com.dashboard.common.service.ICredentialService;
import com.dashboard.common.util.DateTimeUtil;
import com.dashboard.common.util.ReflectionUtil;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Authentication
@Controller
@RequestMapping("/profile")
public class ProfileController extends BaseController {

    @Autowired
    ICredentialService credential;

    @RequestMapping
    public ModelAndView defaultHandler() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("profile");
        Credential credo = credential.GetCredentialInfoByName(getUserSession().getName());
        mav.addObject("credential", credo);
        return mav;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    public Object saveJSONResponse(Credential input, String newDate, String confirm) {
        Map<String, Object> mp = new HashMap<>();
        mp.put("success", true);
        mp.put("message", "Profile has been saved successfully");
        Credential credo = credential.GetCredentialInfoByName(getUserSession().getName());
        input.setUser(getUserSession().getName());
        //check value of birthDate
        Date proposedDate = null;
        try {
            proposedDate = DateTimeUtil.convertStringToDate(newDate, DateTimeUtil.DATE_FORMAT_IND);
            input.setBirthDate(proposedDate);
        } catch (Exception e) {
            mp.put("success", false);
            mp.put("message", "Date format is not correct");
            return mp; //exit the rest of all
        }

        System.out.println("Setelah birthdate ::: \n"+ReflectionUtil.printGetMethodValues(input));
        String cleanCurrentPassword = StringUtils.trimToEmpty(input.getPassword());
        String cleanProposedPassword = StringUtils.trimToEmpty(confirm);

        if (!cleanCurrentPassword.equals(cleanProposedPassword)) {
            mp.put("success", false);
            mp.put("message", "Password and confirmation don't match");
        } else {
            if (cleanProposedPassword.equals("")) {
                mp.put("success", false);
                mp.put("message", "New Password can't be empty");
            } else {
                try {
                    credential.updateCredential(getUserSession().getName(), credo, input);
                } catch (Exception e) {
                    mp.put("success", false);
                    mp.put("message", e.getMessage());
                }

            }
        }
        return mp;
    }
}
