/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dashboard.controller;

import com.dashboard.common.controller.BaseController;
import com.dashboard.common.util.ReflectionUtil;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Genesys8
 */
@Controller
@RequestMapping("/request")
public class RequestController extends BaseController  {
    
    @RequestMapping(value = "/data")
    @ResponseBody
    public String testify(){
        return "sukses jos";
    }
    
    @RequestMapping("/move")
    public String jambu(){
        return "redirect:/request/data"; //new ModelAndView("test");
    }
    
    @RequestMapping("/model")
    public ModelAndView ujicoba(){
        ModelAndView mav = new ModelAndView("test");
        mav.addObject("isLogin",false);
        mav.addObject("username","martinus");
        return mav;
    }
    
    @RequestMapping("/isLogin")
    public String numberoUno(Map<String,Object> model)
    {
        model.put("awesome", "<x:panel>\nI want my patient now\n</x:panel>");
        return "test";
    }
    
    @RequestMapping("/json")
    @ResponseBody
    public Object testJson(HttpServletRequest req){
        Map<String, String> m = new HashMap<String, String>();
        m.put("Page", "1");
        m.put("Verb", "Working/nothing deepr/><bold>");
        m.put("Status", "Succeed");
        return m;
    }
    
    @RequestMapping("/login/{name}")
    public String loginByName(@PathVariable(value = "name")String username,@ModelAttribute("isLogin") boolean isLogin){
        //start a new session
        if(!isLogin){
            super.createSession();
            //set userSession
            super.userSession.setName(username);            
        }
        return "redirect:/request/session";
    }
    
    @RequestMapping("/session")
    @ResponseBody
    public String listSession(){
        return "UserSession Name (Login) :: " + super.userSession.getName();
    }
    
    @RequestMapping("/logout")
    @ResponseBody
    public String logout(){
        //kick out the session
        super.destroySession();
        return ReflectionUtil.printGetMethodValues(super.userSession);
    }
    
            
}
