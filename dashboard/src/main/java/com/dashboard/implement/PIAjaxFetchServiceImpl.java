/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dashboard.implement;

import com.dashboard.common.database.BaseJdbcTemplateDao;
import com.dashboard.service.IPIAjaxFetchService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Service;

/**
 *
 * @author Genesys8
 */
@Service
public class PIAjaxFetchServiceImpl extends BaseJdbcTemplateDao implements IPIAjaxFetchService {

    @Override
    public List<String> getAllSOCustomerName(String keyword) {
        Map<String,String> mp = new HashMap<>();
        mp.put("key", keyword);
        List<String> lst = getNamedJdbcTemplate().queryForList("call sp_getSOCustomer(:key)",mp, String.class);
        return lst;
    }

    @Override
    public List<String> getAllPIID(String keyword) {
        Map<String,String> mp = new HashMap<>();
        mp.put("key", keyword);
        List<String> lst = getNamedJdbcTemplate().queryForList("call sp_getPIID(:key)",mp,String.class);
        return lst;
    }

    @Override
    public List<String> getAllSOID(String keyword) {
        Map<String,String> mp = new HashMap<>();
        mp.put("key", keyword);
        List<String> lst = getNamedJdbcTemplate().queryForList("call sp_getSOID(:key)",mp, String.class);
        return lst;
    }
    
}
