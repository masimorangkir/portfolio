/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dashboard.implement;

import com.dashboard.common.callback.HighchartRowCountCallback;
import com.dashboard.common.callback.HighchartSeriesOnlytRowCountCallback;
import com.dashboard.common.database.BaseJdbcTemplateDao;
import com.dashboard.common.model.HighchartMap;
import com.dashboard.model.ChartPIQtyPrice;
import com.dashboard.model.PITableInfo;
import com.dashboard.model.ReportPIInOut;
import com.dashboard.model.ReportPIInOutWrapper;
import com.dashboard.param.ParamReportPIInOut;
import com.dashboard.service.IReportingPIService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.stereotype.Service;

/**
 *
 * @author Genesys8
 */
@Service
public class ReportingPIServiceImpl extends BaseJdbcTemplateDao implements IReportingPIService {

    @Override
    public ReportPIInOutWrapper getReportGlobal(ParamReportPIInOut paramReport) {
        List<ReportPIInOut> list = getNamedJdbcTemplate().query("call sp_report_global(:paramStart,:paramEnd,:paramPI,:paramSO,:paramCustomer,:paramPageStart,:paramPageLength, :paramSearch);", paramReport.constructMapParameter(), ParameterizedBeanPropertyRowMapper.newInstance(ReportPIInOut.class));
        ReportPIInOutWrapper wrapper = new ReportPIInOutWrapper();
        int totalRow = 0;
        for (ReportPIInOut io : list) {
            //take the total row from one of list of ReportPIInOut
            totalRow = io.getTotalRow();
        }
        wrapper.setTotalRow(totalRow);
        wrapper.setListReports(list);
        return wrapper;

//        getJdbcTemplate().setSkipUndeclaredResults(true);
//        String sql = "{call sp_report_global(?,?,?,?,?)}";
//        // The input parameters of the stored procedure
//        List<SqlParameter> declaredParams = paramReport.constructSqlParameter();
//        CallableStatementCreatorFactory cscFactory = new CallableStatementCreatorFactory(sql, declaredParams);
//
//        // The result sets of the stored procedure
//        List<SqlParameter> returnedParams = Arrays.<SqlParameter>asList(
//            new SqlReturnResultSet("ResultCount", ParameterizedBeanPropertyRowMapper.newInstance(ReportPIInOut.class)),
//            new SqlReturnResultSet("ResultMain", ParameterizedBeanPropertyRowMapper.newInstance(ReportPIInOut.class))
//                );
//        
//        Map<String, Object> actualParams = paramReport.constructMapParameter();
//
//        CallableStatementCreator csc = cscFactory.newCallableStatementCreator(actualParams);
//        Map<String, Object> results = getJdbcTemplate().call(csc, returnedParams);
//        for (Map.Entry<String, Object> entry : results.entrySet()) {
//            //String string = entry.getKey();
//            Object object = entry.getValue();
//            
//        }
    }

    @Override
    public List<ChartPIQtyPrice> getChartQtyPriceByTypeAndKind(String reportKind, String uom) {
        Map<String, String> mp = new HashMap<>();
        mp.put("kind", reportKind);
        mp.put("uom", uom);
        List<ChartPIQtyPrice> lst = getNamedJdbcTemplate().query("call sp_getSumQtyPriceByTypeAndUom(:kind,:uom)", mp, ParameterizedBeanPropertyRowMapper.newInstance(ChartPIQtyPrice.class));
        return lst;
    }

    @Override
    public HighchartMap getChartQtyInOut(String reportKind, String uom, String period, String startDate, String endDate) {
        Map<String, String> mp = new HashMap<>();
        mp.put("kind", reportKind);
        mp.put("uom", uom);
        mp.put("period", period);
        mp.put("startDate", startDate);
        mp.put("endDate", endDate);
        HighchartRowCountCallback callback = new HighchartRowCountCallback();
        getNamedJdbcTemplate().query("call sp_getSumQtyByTypeMatrix(:kind,:uom,:period,:startDate,:endDate)", mp, callback);
        return callback.getHighchartMap();
    }

    /*
     private void insertMap(Map<String, List<BigDecimal>> mp, String key, BigDecimal value) {
     if (!mp.containsKey(key)) {
     List<BigDecimal> newLst = new ArrayList<>();
     newLst.add(value);
     mp.put(key, newLst);
     } else {
     List<BigDecimal> currentLst = mp.get(key);
     currentLst.add(value);
     }
     }

     @Override
     public HighchartMap convertGetChartQtyPriceByTypeAndKind(List<ChartPIQtyPrice> data) {
     HighchartMap hcm = new HighchartMap();

     if (data != null) {
     Map<String, List<BigDecimal>> mpSeries = new HashMap<>();
     List<String> lstPIDate = new ArrayList<>();
     List<String> typeInv = new ArrayList<>();

     //first round
     for (ChartPIQtyPrice cqp : data) {
     //group by PI Date Str
     if (!hcm.getCategories().contains(cqp.getPiDateStr())) {
     hcm.getCategories().add(cqp.getPiDateStr());
     lstPIDate.add(cqp.getPiDateStr());
     }
     //group by Inventory Type
     if (!typeInv.contains(cqp.getTypeId())) {
     typeInv.add(cqp.getTypeId());
     }
     }

     //second round
     int index = 0;
     String currentDate = "";
     List<String> temporary = new ArrayList<>();
     for (ChartPIQtyPrice cup : data) {
     if (index == 0) {
     currentDate = cup.getPiDateStr();
     }
     if (currentDate.equalsIgnoreCase(cup.getPiDateStr())) {
     insertMap(mpSeries, cup.getTypeId(), cup.getQty());
     temporary.add(cup.getTypeId());

     //if it's the last iteration
     if (index == data.size() - 1) {
     for (String loopType : typeInv) {
     if (!temporary.contains(loopType)) {
     insertMap(mpSeries, loopType, BigDecimal.ZERO);
     }
     }
     }
     } else { //if currentDate is not equal with cup.getPIDateStr()

     //add all remaining inventory type to map
     for (String loopType : typeInv) {
     //only insert into map if remaining type not already inserted into temporary variable
     if (!temporary.contains(loopType)) {
     insertMap(mpSeries, loopType, BigDecimal.ZERO);
     }
     }

     //clear the content of temporary variable
     temporary.clear();

     //insert into map + temporary variable
     insertMap(mpSeries, cup.getTypeId(), cup.getQty());
     temporary.add(cup.getTypeId());

     //update the value of currentDate
     currentDate = cup.getPiDateStr();
     }
     index++;
     }

     for (Map.Entry<String, List<BigDecimal>> entry : mpSeries.entrySet()) {
     String string = entry.getKey();
     List<BigDecimal> list = entry.getValue();
     HighchartSeries series = new HighchartSeries();
     series.setName(string);
     series.setData(list);
     hcm.getSeries().add(series);
     }
     }

     return hcm;
     }
     */
    @Override
    public HighchartMap getChartQtyTopTenByInventoryId(String reportKind, String startDate, String endDate) {
        Map<String, String> mp = new HashMap<>();
        mp.put("kind", reportKind);
        mp.put("startDate",startDate);
        mp.put("endDate",endDate);
        HighchartSeriesOnlytRowCountCallback only = new HighchartSeriesOnlytRowCountCallback(1, 3);
        only.setHorizontalTitle("Qty "+ StringUtils.capitalize(reportKind));
        getNamedJdbcTemplate().query("call sp_getInventoryQtyTopTen(:kind,:startDate,:endDate)", mp, only);
        return only.getHighchartMap();
    }

    @Override
    public HighchartMap getChartPriceTopTenByInventoryId(String reportKind, String startDate, String endDate) {
        Map<String, String> mp = new HashMap<>();
        mp.put("kind", reportKind);
        mp.put("startDate",startDate);
        mp.put("endDate",endDate);
        HighchartSeriesOnlytRowCountCallback only = new HighchartSeriesOnlytRowCountCallback(1, 3);
        only.setHorizontalTitle("Price "+ StringUtils.capitalize(reportKind));
        getNamedJdbcTemplate().query("call sp_getInventoryPriceTopTen(:kind,:startDate,:endDate)", mp, only);
        return only.getHighchartMap();
    }

    @Override
    public List<PITableInfo> getListQtyTopTenByInventoryName(String reportKind, String startDate, String endDate) {
        Map<String, String> mp = new HashMap<>();
        mp.put("kind", reportKind);
        mp.put("startDate", startDate);
        mp.put("endDate", endDate);
        List<PITableInfo> lst = getNamedJdbcTemplate().query("call sp_getInventoryQtyTopTen(:kind,:startDate,:endDate)", mp, ParameterizedBeanPropertyRowMapper.newInstance(PITableInfo.class));
        return lst;
    }

    @Override
    public List<PITableInfo> getListPriceTopTenByInventoryName(String reportKind, String startDate, String endDate) {
        Map<String, String> mp = new HashMap<>();
        mp.put("kind", reportKind);
        mp.put("startDate", startDate);
        mp.put("endDate", endDate);
        List<PITableInfo> lst = getNamedJdbcTemplate().query("call sp_getInventoryPriceTopTen(:kind,:startDate,:endDate)", mp, ParameterizedBeanPropertyRowMapper.newInstance(PITableInfo.class));
        return lst;
    }
}
