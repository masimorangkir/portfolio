/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dashboard.model;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Genesys8
 */
public class ChartPIQtyPrice {
    private Date piDate;
    private String piDateStr;
    private String typeId;
    private BigDecimal qty;
    private BigDecimal price;

    public Date getPiDate() {
        return piDate;
    }

    public void setPiDate(Date piDate) {
        this.piDate = piDate;
    }

    public String getPiDateStr() {
        return piDateStr;
    }

    public void setPiDateStr(String piDateStr) {
        this.piDateStr = piDateStr;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public BigDecimal getQty() {
        return qty;
    }

    public void setQty(BigDecimal qty) {
        this.qty = qty;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
    
}
