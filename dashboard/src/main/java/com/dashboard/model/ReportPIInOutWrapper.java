/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dashboard.model;

import com.dashboard.model.ReportPIInOut;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Genesys8
 */
public class ReportPIInOutWrapper {
    private int totalRow;
    private List<ReportPIInOut> listReports = new ArrayList<>();

    public int getTotalRow() {
        return totalRow;
    }

    public void setTotalRow(int totalRow) {
        this.totalRow = totalRow;
    }

    public List<ReportPIInOut> getListReports() {
        return listReports;
    }

    public void setListReports(List<ReportPIInOut> listReports) {
        this.listReports = listReports;
    }
    
}
