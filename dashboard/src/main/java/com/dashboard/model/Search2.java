/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dashboard.model;

/**
 *
 * @author Genesys8
 */
public class Search2 {
    private String text;
    private String id;
    
    public Search2(){
        
    }
    public Search2(String textAndId){
        this.text = textAndId;
        this.id = textAndId;
    }
    public Search2(String text, String id){
        this.text = text;
        this.id = id;
    }
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
}
