/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dashboard.param;

import java.sql.Types;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.SqlParameter;

/**
 *
 * @author Genesys8
 */
public class ParamReportPIInOut {

    private String paramStart;
    private String paramEnd;
    private String paramPiid;
    private String paramSoid;
    private String paramCustomerName;
    private String paramSearch;
    private int paramPageStart;
    private int paramPageLength;

    public String getParamSearch() {
        return StringUtils.defaultIfBlank(paramSearch,"");
    }

    public void setParamSearch(String paramSearch) {
        this.paramSearch = paramSearch;
    }
    
    public int getParamPageStart() {
        return paramPageStart;
    }

    public void setParamPageStart(int paramPageStart) {
        this.paramPageStart = paramPageStart;
    }

    public int getParamPageLength() {
        return paramPageLength;
    }

    public void setParamPageLength(int pageLength) {
        this.paramPageLength = pageLength;
    }
    
    public String getParamStart() {
        return StringUtils.defaultIfBlank(paramStart,"");
    }

    public void setParamStart(String paramStart) {
        this.paramStart = paramStart;
    }

    public String getParamEnd() {
        return StringUtils.defaultIfBlank(paramEnd,"");
    }

    public void setParamEnd(String paramEnd) {
        this.paramEnd = paramEnd;
    }

    public String getParamPiid() {
        return StringUtils.defaultIfBlank(paramPiid,"");
    }

    public void setParamPiid(String paramPiid) {
        this.paramPiid = paramPiid;
    }

    public String getParamSoid() {
        return StringUtils.defaultIfBlank(paramSoid,"");
    }

    public void setParamSoid(String paramSoid) {
        this.paramSoid = paramSoid;
    }

    public String getParamCustomerName() {
        return StringUtils.defaultIfBlank(paramCustomerName,"");
    }

    public void setParamCustomerName(String paramCustomerName) {
        this.paramCustomerName = paramCustomerName;
    }

    public Map<String, Object> constructMapParameter() {
        Map<String, Object> m = new HashMap<String, Object>();
        m.put("paramStart", this.getParamStart());
        m.put("paramEnd", this.getParamEnd());
        m.put("paramPI", this.getParamPiid());
        m.put("paramSO", this.getParamSoid());
        m.put("paramCustomer", this.getParamCustomerName());
        m.put("paramPageStart", this.getParamPageStart());
        m.put("paramPageLength", this.getParamPageLength());
        m.put("paramSearch", this.getParamSearch());
        return m;
    }
    public List<SqlParameter> constructSqlParameter(){
        List<SqlParameter> lst= Arrays.asList(
           new SqlParameter("paramStart", Types.VARCHAR),
           new SqlParameter("paramEnd", Types.VARCHAR),
           new SqlParameter("paramPI", Types.VARCHAR),
           new SqlParameter("paramSO", Types.VARCHAR),
           new SqlParameter("paramCustomer", Types.VARCHAR)                
        );
        return lst;
    }
}
