/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dashboard.service;

import java.util.List;

/**
 *
 * @author Genesys8
 */
public interface IPIAjaxFetchService {
    public List<String> getAllSOCustomerName(String keyword);
    public List<String> getAllPIID(String keyword);
    public List<String> getAllSOID(String keyword);
}
