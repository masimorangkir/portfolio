/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dashboard.service;

import com.dashboard.common.model.HighchartMap;
import com.dashboard.model.ChartPIQtyPrice;
import com.dashboard.model.PITableInfo;
import com.dashboard.model.ReportPIInOutWrapper;
import com.dashboard.param.ParamReportPIInOut;
import java.util.List;
import java.util.regex.Pattern;

/**
 *
 * @author Genesys8
 */
public interface IReportingPIService {
    ReportPIInOutWrapper  getReportGlobal(ParamReportPIInOut paramReport);
    List<ChartPIQtyPrice> getChartQtyPriceByTypeAndKind(String reportKind, String uom);
    HighchartMap getChartQtyInOut(String reportKind, String uom, String period, String startDate, String endDate);
    HighchartMap getChartQtyTopTenByInventoryId(String reportKind, String startDate, String endDate);
    HighchartMap getChartPriceTopTenByInventoryId(String reportKind, String startDate, String endDate);
    List<PITableInfo> getListQtyTopTenByInventoryName(String reportKind, String startDate, String endDate);
    List<PITableInfo> getListPriceTopTenByInventoryName(String reportKind, String startDate, String endDate);
}
