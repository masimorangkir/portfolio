package com.dashboard.test;

import com.dashboard.common.database.BaseJdbcTemplateDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.jdbc.core.RowCountCallbackHandler;
import org.springframework.stereotype.Service;

@Service
public class DB extends BaseJdbcTemplateDao {

    CallbackHandler ch = new CallbackHandler();

    public void measure() {
        getJdbcTemplate().query("select * from t_detail_in_out", ch);
        String[] cols = ch.getColumnNames();
        for (String col : cols) {
            System.out.println("Column Name :: " + col);
        }
        for (String dt : ch.getList()) {
            System.out.println("Isi list :: " + dt);
        }
    }
}

class CallbackHandler extends RowCountCallbackHandler {

    int count = 0;
    List<String> data = new ArrayList<>();

    @Override
    protected void processRow(ResultSet rs, int rowNum) throws SQLException {
        
        data.add(rs.getString(2));
        //super.processRow(rs, rowNum); //To change body of generated methods, choose Tools | Templates.
    }

    public List<String> getList() {
        return this.data;
    }

}
