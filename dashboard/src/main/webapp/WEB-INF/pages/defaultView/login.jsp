<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Dashboard Login</title>
        <link type="text/css" rel="stylesheet" href="<s:url value="/bundles/entry.css"/>"/>
    </head>
    <body>
        <div class="login-container animated fadeInDown">
            <div class="">
                <div class="widget">
                    <div class="widget-header bordered-bottom bordered-blue">
                        <span class="widget-caption">Dashboard Login</span>
                    </div>
                    <div class="widget-body">
                        <div>
                            <form role="form" action="<s:url value="/login"/>" method="POST">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Username</label>
                                    <input type="text" class="form-control" id="user" name="user" placeholder="username">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Password</label>
                                    <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                                </div>
                                <c:if test="${not empty error}">
                                    <div class="form-group">
                                        <div class="alert alert-danger">${error}</div>
                                    </div>
                                </c:if>
                                <button type="submit" class="btn btn-blue">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="widget">
                    <div class="widget-header bordered-bottom bordered-blue">
                        <span class="widget-caption"><i class="glyphicon glyphicon-check"></i>&nbsp;Information</span>
                    </div>    
                    <div class="widget-body">Presented by : 
                        <div>
                            <address><br>
                                <i class="glyphicon glyphicon-user"></i>&nbsp;<strong>Martinus Ardianto</strong> <br>
                                <i class="glyphicon glyphicon-phone"></i>&nbsp;(+62)888-01626-440<br>
                                <i class="glyphicon glyphicon-envelope"></i>&nbsp;<a href="mailto:martinus.ardianto@yahoo.com">martinus.ardianto@yahoo.com</a>
                            </address>
                        </div>
                        <div>
                            I've already hosted this website's source code at <a href="https://bitbucket.org/masimorangkir/portfolio">Bitbucket.org</a>, please visit that link if you are interested in reviewing how 'good' or 'bad'my code writing skill is.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="<s:url value="/bundles/entry.js"/>"></script>
    </body>
</html>
