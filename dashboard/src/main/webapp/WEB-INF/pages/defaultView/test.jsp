<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page language="java"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="f" uri="/WEB-INF/tld/dashboard.tld" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
        <link type="text/css" rel="stylesheet" href="<s:url value="/bundles/whitelabel.css"/>"/>
        <title>Bootstrap Page</title>
    </head>
    <body class="skin-blue">

       
        
        <div id="navbar" class="navbar navbar-fixed">
            <button type="button" class="navbar-toggle navbar-btn collapsed" data-toggle="collapse" data-target="#sidebar">
                <span class="fa fa-bars"></span>
            </button>
            <a class="navbar-brand" href="#">
                <small>
                    <i class="fa fa-desktop"></i>
                    ${getCompanySetting.name} Dashboard
                </small>
            </a>

            <!-- BEGIN Navbar Buttons -->
            <ul class="nav flaty-nav pull-right">
                <!-- BEGIN Button Tasks -->
                <li class="hidden-xs">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="fa fa-tasks"></i>
                        <span class="badge badge-warning">4</span>
                    </a>

                    <!-- BEGIN Tasks Dropdown -->
                    <ul class="dropdown-navbar dropdown-menu">
                        <li class="nav-header">
                            <i class="fa fa-check"></i>
                            4 Tasks to complete
                        </li>

                        <li>
                            <a href="#">
                                <div class="clearfix">
                                    <span class="pull-left">Software Update</span>
                                    <span class="pull-right">75%</span>
                                </div>

                                <div class="progress progress-mini">
                                    <div style="width:75%" class="progress-bar progress-bar-warning"></div>
                                </div>
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <div class="clearfix">
                                    <span class="pull-left">Transfer To New Server</span>
                                    <span class="pull-right">45%</span>
                                </div>

                                <div class="progress progress-mini">
                                    <div style="width:45%" class="progress-bar progress-bar-danger"></div>
                                </div>
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <div class="clearfix">
                                    <span class="pull-left">Bug Fixes</span>
                                    <span class="pull-right">20%</span>
                                </div>

                                <div class="progress progress-mini">
                                    <div style="width:20%" class="progress-bar"></div>
                                </div>
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <div class="clearfix">
                                    <span class="pull-left">Writing Documentation</span>
                                    <span class="pull-right">85%</span>
                                </div>

                                <div class="progress progress-mini progress-striped active">
                                    <div style="width:85%" class="progress-bar progress-bar-success"></div>
                                </div>
                            </a>
                        </li>

                        <li class="more">
                            <a href="#">See tasks with details</a>
                        </li>
                    </ul>
                    <!-- END Tasks Dropdown -->
                </li>
                <!-- END Button Tasks -->

                <!-- BEGIN Button Notifications -->
                <li class="hidden-xs">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="fa fa-bell anim-swing"></i>
                        <span class="badge badge-important">5</span>
                    </a>

                    <!-- BEGIN Notifications Dropdown -->
                    <ul class="dropdown-navbar dropdown-menu">
                        <li class="nav-header">
                            <i class="fa fa-warning"></i>
                            5 Notifications
                        </li>

                        <li class="notify">
                            <a href="#">
                                <i class="fa fa-comment orange"></i>
                                <p>New Comments</p>
                                <span class="badge badge-warning">4</span>
                            </a>
                        </li>

                        <li class="notify">
                            <a href="#">
                                <i class="fa fa-twitter blue"></i>
                                <p>New Twitter followers</p>
                                <span class="badge badge-info">7</span>
                            </a>
                        </li>

                        <li class="notify">
                            <a href="#">
                                <img src="img/demo/avatar/avatar2.jpg" alt="Alex">
                                <p>David would like to become moderator.</p>
                            </a>
                        </li>

                        <li class="notify">
                            <a href="#">
                                <i class="fa fa-bug pink"></i>
                                <p>New bug in program!</p>
                            </a>
                        </li>

                        <li class="notify">
                            <a href="#">
                                <i class="fa fa-shopping-cart green"></i>
                                <p>You have some new orders</p>
                                <span class="badge badge-success">+10</span>
                            </a>
                        </li>

                        <li class="more">
                            <a href="#">See all notifications</a>
                        </li>
                    </ul>
                    <!-- END Notifications Dropdown -->
                </li>
                <!-- END Button Notifications -->

                <!-- BEGIN Button Messages -->
                <li class="hidden-xs">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="fa fa-envelope anim-top-down"></i>
                        <span class="badge badge-success">3</span>
                    </a>

                    <!-- BEGIN Messages Dropdown -->
                    <ul class="dropdown-navbar dropdown-menu">
                        <li class="nav-header">
                            <i class="fa fa-comments"></i>
                            3 Messages
                        </li>

                        <li class="msg">
                            <a href="#">
                                <img src="img/demo/avatar/avatar3.jpg" alt="Sarah's Avatar">
                                <div>
                                    <span class="msg-title">Sarah</span>
                                    <span class="msg-time">
                                        <i class="fa fa-clock-o"></i>
                                        <span>a moment ago</span>
                                    </span>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </a>
                        </li>

                        <li class="msg">
                            <a href="#">
                                <img src="img/demo/avatar/avatar4.jpg" alt="Emma's Avatar">
                                <div>
                                    <span class="msg-title">Emma</span>
                                    <span class="msg-time">
                                        <i class="fa fa-clock-o"></i>
                                        <span>2 Days ago</span>
                                    </span>
                                </div>
                                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ...</p>
                            </a>
                        </li>

                        <li class="msg">
                            <a href="#">
                                <img src="img/demo/avatar/avatar5.jpg" alt="John's Avatar">
                                <div>
                                    <span class="msg-title">John</span>
                                    <span class="msg-time">
                                        <i class="fa fa-clock-o"></i>
                                        <span>8:24 PM</span>
                                    </span>
                                </div>
                                <p>Duis aute irure dolor in reprehenderit in ...</p>
                            </a>
                        </li>

                        <li class="more">
                            <a href="#">See all messages</a>
                        </li>
                    </ul>
                    <!-- END Notifications Dropdown -->
                </li>
                <!-- END Button Messages -->

                <!-- BEGIN Button User -->
                <li class="user-profile">
                    <a data-toggle="dropdown" href="#" class="user-menu dropdown-toggle">
                        <img class="nav-user-photo" src="img/demo/avatar/avatar1.jpg" alt="Penny's Photo">
                        <span id="user_info">
                            Penny
                        </span>
                        <i class="fa fa-caret-down"></i>
                    </a>

                    <!-- BEGIN User Dropdown -->
                    <ul class="dropdown-menu dropdown-navbar" id="user_menu">
                        <li class="nav-header">
                            <i class="fa fa-clock-o"></i>
                            Logined From 20:45
                        </li>

                        <li>
                            <a href="#">
                                <i class="fa fa-cog"></i>
                                Account Settings
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <i class="fa fa-user"></i>
                                Edit Profile
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <i class="fa fa-question"></i>
                                Help
                            </a>
                        </li>

                        <li class="divider visible-xs"></li>

                        <li class="visible-xs">
                            <a href="#">
                                <i class="fa fa-tasks"></i>
                                Tasks
                                <span class="badge badge-warning">4</span>
                            </a>
                        </li>
                        <li class="visible-xs">
                            <a href="#">
                                <i class="fa fa-bell"></i>
                                Notifications
                                <span class="badge badge-important">8</span>
                            </a>
                        </li>
                        <li class="visible-xs">
                            <a href="#">
                                <i class="fa fa-envelope"></i>
                                Messages
                                <span class="badge badge-success">5</span>
                            </a>
                        </li>

                        <li class="divider"></li>

                        <li>
                            <a href="#">
                                <i class="fa fa-off"></i>
                                Logout
                            </a>
                        </li>
                    </ul>
                    <!-- BEGIN User Dropdown -->
                </li>
                <!-- END Button User -->
            </ul>
            <!-- END Navbar Buttons -->
        </div>


        <div class="container" id="main-container">
            <!-- BEGIN Sidebar -->
            <div id="sidebar" class="navbar-collapse collapse sidebar-fixed">
                <!-- BEGIN Navlist -->
                <ul class="nav nav-list" style="height: auto;">           
                    <f:menuItem icon="fa-dashboard" link="demo.html" idComponent="dashboard">Dashboard</f:menuItem>
                    <f:menuItem icon="fa-rocket" link="demo.html">Rocketeer</f:menuItem>

                    <f:subMenuWrap caption="Utility Tools" idComponent="001" icon="fa-bolt">
                        <f:subMenuItem link="gandoz.html" idComponent="001_2">Luwes Gan</f:subMenuItem>
                        <f:subMenuItemGroup caption="MultiLevel">
                            <f:subMenuItem link="wenak.htm">Sambal</f:subMenuItem>
                            <f:subMenuItem link="wenak.htm" idComponent="001_3_2">Terasi</f:subMenuItem>
                        </f:subMenuItemGroup>
                    </f:subMenuWrap>        
                    <f:menuItem>Awesomeness</f:menuItem>
                    <f:menuItem icon="fa-dashboard" link="demo.html" idComponent="dashboard">Dashboard</f:menuItem>
                    <f:menuItem icon="fa-rocket" link="demo.html">Rocketeer</f:menuItem>

                    <f:subMenuWrap caption="Utility Tools" idComponent="001" icon="fa-bolt">
                        <f:subMenuItem link="gandoz.html" idComponent="001_2">Luwes Gan</f:subMenuItem>
                        <f:subMenuItemGroup caption="MultiLevel">
                            <f:subMenuItem link="wenak.htm">Sambal</f:subMenuItem>
                            <f:subMenuItem link="wenak.htm" idComponent="001_3_2">Terasi</f:subMenuItem>
                        </f:subMenuItemGroup>
                    </f:subMenuWrap>        
                    <f:menuItem>Awesomeness</f:menuItem>
                    <f:menuItem icon="fa-dashboard" link="demo.html" idComponent="dashboard">Dashboard</f:menuItem>
                    <f:menuItem icon="fa-rocket" link="demo.html">Rocketeer</f:menuItem>

                    <f:subMenuWrap caption="Utility Tools" idComponent="001" icon="fa-bolt">
                        <f:subMenuItem link="gandoz.html" idComponent="001_2">Luwes Gan</f:subMenuItem>
                        <f:subMenuItemGroup caption="MultiLevel">
                            <f:subMenuItem link="wenak.htm">Sambal</f:subMenuItem>
                            <f:subMenuItem link="wenak.htm" idComponent="001_3_2">Terasi</f:subMenuItem>
                        </f:subMenuItemGroup>
                    </f:subMenuWrap>        
                    <f:menuItem>Awesomeness</f:menuItem>
                    <f:menuItem icon="fa-dashboard" link="demo.html" idComponent="dashboard">Dashboard</f:menuItem>
                    <f:menuItem icon="fa-rocket" link="demo.html">Rocketeer</f:menuItem>

                    <f:subMenuWrap caption="Utility Tools" idComponent="001" icon="fa-bolt">
                        <f:subMenuItem link="gandoz.html" idComponent="001_2">Luwes Gan</f:subMenuItem>
                        <f:subMenuItemGroup caption="MultiLevel">
                            <f:subMenuItem link="wenak.htm">Sambal</f:subMenuItem>
                            <f:subMenuItem link="wenak.htm" idComponent="001_3_2">Terasi</f:subMenuItem>
                        </f:subMenuItemGroup>
                    </f:subMenuWrap>        
                    <f:menuItem>Awesomeness</f:menuItem>
                    <f:menuItem icon="fa-dashboard" link="demo.html" idComponent="dashboard">Dashboard</f:menuItem>
                    <f:menuItem icon="fa-rocket" link="demo.html">Rocketeer</f:menuItem>

                    <f:subMenuWrap caption="Utility Tools" idComponent="001" icon="fa-bolt">
                        <f:subMenuItem link="gandoz.html" idComponent="001_2">Luwes Gan</f:subMenuItem>
                        <f:subMenuItemGroup caption="MultiLevel">
                            <f:subMenuItem link="wenak.htm">Sambal</f:subMenuItem>
                            <f:subMenuItem link="wenak.htm" idComponent="001_3_2">Terasi</f:subMenuItem>
                        </f:subMenuItemGroup>
                    </f:subMenuWrap>        
                    <f:menuItem>Awesomeness</f:menuItem>
                    <f:menuItem icon="fa-dashboard" link="demo.html" idComponent="dashboard">Dashboard</f:menuItem>
                    <f:menuItem icon="fa-rocket" link="demo.html">Rocketeer</f:menuItem>

                    <f:subMenuWrap caption="Utility Tools" idComponent="001" icon="fa-bolt">
                        <f:subMenuItem link="gandoz.html" idComponent="001_2">Luwes Gan</f:subMenuItem>
                        <f:subMenuItemGroup caption="MultiLevel">
                            <f:subMenuItem link="wenak.htm">Sambal</f:subMenuItem>
                            <f:subMenuItem link="wenak.htm" idComponent="001_3_2">Terasi</f:subMenuItem>
                        </f:subMenuItemGroup>
                    </f:subMenuWrap>        
                    <f:menuItem>Awesomeness</f:menuItem>
                    <f:menuItem icon="fa-dashboard" link="demo.html" idComponent="dashboard">Dashboard</f:menuItem>
                    <f:menuItem icon="fa-rocket" link="demo.html">Rocketeer</f:menuItem>

                    <f:subMenuWrap caption="Utility Tools" idComponent="001" icon="fa-bolt">
                        <f:subMenuItem link="gandoz.html" idComponent="001_2">Luwes Gan</f:subMenuItem>
                        <f:subMenuItemGroup caption="MultiLevel">
                            <f:subMenuItem link="wenak.htm">Sambal</f:subMenuItem>
                            <f:subMenuItem link="wenak.htm" idComponent="001_3_2">Terasi</f:subMenuItem>
                        </f:subMenuItemGroup>
                    </f:subMenuWrap>        
                    <f:menuItem>Awesomeness</f:menuItem>
                    <f:menuItem icon="fa-dashboard" link="demo.html" idComponent="dashboard">Dashboard</f:menuItem>
                    <f:menuItem icon="fa-rocket" link="demo.html">Rocketeer</f:menuItem>

                    <f:subMenuWrap caption="Utility Tools" idComponent="001" icon="fa-bolt">
                        <f:subMenuItem link="gandoz.html" idComponent="001_2">Luwes Gan</f:subMenuItem>
                        <f:subMenuItemGroup caption="MultiLevel">
                            <f:subMenuItem link="wenak.htm">Sambal</f:subMenuItem>
                            <f:subMenuItem link="wenak.htm" idComponent="001_3_2">Terasi</f:subMenuItem>
                        </f:subMenuItemGroup>
                    </f:subMenuWrap>        
                    <f:menuItem>Awesomeness</f:menuItem>
                    </ul>
                    <!-- END Navlist -->

                    <!-- BEGIN Sidebar Collapse Button -->
                    <div id="sidebar-collapse" class="visible-lg">
                        <i class="fa fa-angle-double-left"></i>
                    </div>
                    <!-- END Sidebar Collapse Button -->
                </div>
                <!-- END Sidebar -->




                <!-- BEGIN Content -->
                <div id="main-content">
                    <!-- BEGIN Page Title -->
                    <div class="page-title">
                        <div>
                            <h1><i class="fa fa-file-o"></i> Form Layout</h1>
                            <h4>Simple form element, griding and layout</h4>
                        </div>
                    </div>
                    <!-- END Page Title -->

                    <!-- BEGIN Breadcrumb -->
                    <div id="breadcrumbs">
                        <ul class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="index.html">Home</a>
                                <span class="divider"><i class="fa fa-angle-right"></i></span>
                            </li>
                            <li class="active">Form Layout</li>
                        </ul>
                    </div>
                    <!-- END Breadcrumb -->
                    <div  class="row">
                        <div class="col-md-12">

                            
                        </div>
                    </div>
                    <!-- BEGIN Main Content -->
                    <div class="row">
                     
                    </div>

                    <!-- END Main Content -->

                    
                    <div class="row">          
                    <f:boxWrap boxColor="black" columnLayoutClass="col-md-5">
                        <f:boxTitleAndToolWrap caption="Purchase Order" icon="fa-bolt" useClose="true" useCollapse="true">
                           
                        </f:boxTitleAndToolWrap> 
                        <f:boxContentWrap className="weekly-stats" useListStyle="true" useSlimScroll="true">
                            <li>
                                <span class="inline-sparkline"><canvas width="70" height="26" style="display: inline-block; width: 70px; height: 26px; vertical-align: top;"></canvas></span>
                                Visits: <span class="value">376</span>
                            </li>
                            <li>
                                <span class="inline-sparkline"><canvas width="70" height="26" style="display: inline-block; width: 70px; height: 26px; vertical-align: top;"></canvas></span>
                                Unique Visitors: <span class="value">238</span>
                            </li>
                            <li>
                                <span class="inline-sparkline"><canvas width="70" height="26" style="display: inline-block; width: 70px; height: 26px; vertical-align: top;"></canvas></span>
                                Page Views: <span class="value">514</span>
                            </li>
                            <li>
                                <span class="inline-sparkline"><canvas width="70" height="26" style="display: inline-block; width: 70px; height: 26px; vertical-align: top;"></canvas></span>
                                Pages / Visit: <span class="value">1.43</span>
                            </li>
                            <li>
                                <span class="inline-sparkline"><canvas width="70" height="26" style="display: inline-block; width: 70px; height: 26px; vertical-align: top;"></canvas></span>
                                Avg. Visit Time: <span class="value">00:02:34</span>
                            </li>
                            <li>
                                <span class="inline-sparkline"><canvas width="70" height="26" style="display: inline-block; width: 70px; height: 26px; vertical-align: top;"></canvas></span>
                                Bounce Rate: <span class="value">73.56%</span>
                            </li>
                            <li>
                                <span class="inline-sparkline"><canvas width="70" height="26" style="display: inline-block; width: 70px; height: 26px; vertical-align: top;"></canvas></span>
                                % New Visits: <span class="value">82.65%</span>
                            </li>
                        </f:boxContentWrap>
                    </f:boxWrap>
                    <f:boxWrap boxColor="magenta" columnLayoutClass="col-md-7">
                        <f:boxTitleAndToolWrap caption="GIla" icon="fa-bolt" useClose="true"></f:boxTitleAndToolWrap>
                        <f:boxContentWrap className="tile" >
                            Wkwkwk asd;fkajsd;fkja s;dfk ja;sd f;d j;fldlajksd;a jd;flsda fja; fja; ask;fldjka sd; fjas; fjd;sfjka sfja;ldf
                            asdfaskdjf; ;akjsd;fjk asfjsa;jk as; jkas; fjsd
                        </f:boxContentWrap>
                    </f:boxWrap>
               
                </div>
                <div class="row">
                    <f:boxWrap boxColor="pink" columnLayoutClass="col-md-4">
                        <f:boxTitleAndToolWrap caption="Ga enak" icon="fa-magic" useClose="true" useCollapse="true"></f:boxTitleAndToolWrap>
                        <f:boxContentWrap className="weekday" useSlimScroll="true" >
                            ear (alias) fa-gears (alias) fa-gift fa-glass fa-globe fa-group (alias) fa-hdd-o fa-headphones fa-heart fa-heart-o fa-home fa-inbox fa-info fa-info-circle fa-key fa-keyboard-o fa-laptop fa-leaf fa-legal (alias) fa-lemon-o fa-level-down fa-level-up fa-lightbulb-o fa-location-arrow fa-lock fa-magic fa-magnet fa-mail-forward (alias) fa-mail-reply (alias) fa-mail-reply-all fa-male fa-map-marker fa-meh-o fa-microphone fa-microphone-slash fa-minus fa-minus-circle fa-minus-square fa-minus-square-o fa-mobile fa-mobile-phone (alias) fa-money fa-moon-o fa-music fa-pencil fa-pencil-square fa-pencil-square-o fa-phone fa-phone-square fa-picture-o fa-plane fa-plus fa-plus-circle fa-plus-square
                        </f:boxContentWrap>
                    </f:boxWrap>
                    <f:boxWrap boxColor="blue" columnLayoutClass="col-md-4">
                        <f:boxTitleAndToolWrap caption="Ga enak" icon="fa-magic" useClose="true" useCollapse="true"></f:boxTitleAndToolWrap>
                        <f:boxContentWrap className="weekday" useSlimScroll="true" >
                            ear (alias) fa-gears (alias) fa-gift fa-glass fa-globe fa-group (alias) fa-hdd-o fa-headphones fa-heart fa-heart-o fa-home fa-inbox fa-info fa-info-circle fa-key fa-keyboard-o fa-laptop fa-leaf fa-legal (alias) fa-lemon-o fa-level-down fa-level-up fa-lightbulb-o fa-location-arrow fa-lock fa-magic fa-magnet fa-mail-forward (alias) fa-mail-reply (alias) fa-mail-reply-all fa-male fa-map-marker fa-meh-o fa-microphone fa-microphone-slash fa-minus fa-minus-circle fa-minus-square fa-minus-square-o fa-mobile fa-mobile-phone (alias) fa-money fa-moon-o fa-music fa-pencil fa-pencil-square fa-pencil-square-o fa-phone fa-phone-square fa-picture-o fa-plane fa-plus fa-plus-circle fa-plus-square
                        </f:boxContentWrap>
                    </f:boxWrap>
                    <f:boxWrap boxColor="red" columnLayoutClass="col-md-4">
                        <f:boxTitleAndToolWrap caption="Ga enak" icon="fa-magic" useClose="true" useCollapse="true"></f:boxTitleAndToolWrap>
                        <f:boxContentWrap className="weekday" useSlimScroll="true" >
                            ear (alias) fa-gears (alias) fa-gift fa-glass fa-globe fa-group (alias) fa-hdd-o fa-headphones fa-heart fa-heart-o fa-home fa-inbox fa-info fa-info-circle fa-key fa-keyboard-o fa-laptop fa-leaf fa-legal (alias) fa-lemon-o fa-level-down fa-level-up fa-lightbulb-o fa-location-arrow fa-lock fa-magic fa-magnet fa-mail-forward (alias) fa-mail-reply (alias) fa-mail-reply-all fa-male fa-map-marker fa-meh-o fa-microphone fa-microphone-slash fa-minus fa-minus-circle fa-minus-square fa-minus-square-o fa-mobile fa-mobile-phone (alias) fa-money fa-moon-o fa-music fa-pencil fa-pencil-square fa-pencil-square-o fa-phone fa-phone-square fa-picture-o fa-plane fa-plus fa-plus-circle fa-plus-square
                        </f:boxContentWrap>
                    </f:boxWrap>
                </div>
                <div class="row">
                    <f:boxWrap boxColor="pink" columnLayoutClass="col-md-4">
                        <f:boxTitleAndToolWrap caption="Ga enak" icon="fa-magic" useClose="true" useCollapse="true"></f:boxTitleAndToolWrap>
                        <f:boxContentWrap className="weekday" useSlimScroll="true" >
                            ear (alias) fa-gears (alias) fa-gift fa-glass fa-globe fa-group (alias) fa-hdd-o fa-headphones fa-heart fa-heart-o fa-home fa-inbox fa-info fa-info-circle fa-key fa-keyboard-o fa-laptop fa-leaf fa-legal (alias) fa-lemon-o fa-level-down fa-level-up fa-lightbulb-o fa-location-arrow fa-lock fa-magic fa-magnet fa-mail-forward (alias) fa-mail-reply (alias) fa-mail-reply-all fa-male fa-map-marker fa-meh-o fa-microphone fa-microphone-slash fa-minus fa-minus-circle fa-minus-square fa-minus-square-o fa-mobile fa-mobile-phone (alias) fa-money fa-moon-o fa-music fa-pencil fa-pencil-square fa-pencil-square-o fa-phone fa-phone-square fa-picture-o fa-plane fa-plus fa-plus-circle fa-plus-square
                        </f:boxContentWrap>
                    </f:boxWrap>
                    <f:boxWrap boxColor="blue" columnLayoutClass="col-md-4">
                        <f:boxTitleAndToolWrap caption="Ga enak" icon="fa-magic" useClose="true" useCollapse="true"></f:boxTitleAndToolWrap>
                        <f:boxContentWrap className="weekday" useSlimScroll="true" >
                            ear (alias) fa-gears (alias) fa-gift fa-glass fa-globe fa-group (alias) fa-hdd-o fa-headphones fa-heart fa-heart-o fa-home fa-inbox fa-info fa-info-circle fa-key fa-keyboard-o fa-laptop fa-leaf fa-legal (alias) fa-lemon-o fa-level-down fa-level-up fa-lightbulb-o fa-location-arrow fa-lock fa-magic fa-magnet fa-mail-forward (alias) fa-mail-reply (alias) fa-mail-reply-all fa-male fa-map-marker fa-meh-o fa-microphone fa-microphone-slash fa-minus fa-minus-circle fa-minus-square fa-minus-square-o fa-mobile fa-mobile-phone (alias) fa-money fa-moon-o fa-music fa-pencil fa-pencil-square fa-pencil-square-o fa-phone fa-phone-square fa-picture-o fa-plane fa-plus fa-plus-circle fa-plus-square
                        </f:boxContentWrap>
                    </f:boxWrap>
                    <f:boxWrap boxColor="red" columnLayoutClass="col-md-4">
                        <f:boxTitleAndToolWrap caption="Ga enak" icon="fa-magic" useClose="true" useCollapse="true"></f:boxTitleAndToolWrap>
                        <f:boxContentWrap className="weekday" useSlimScroll="true" >
                            ear (alias) fa-gears (alias) fa-gift fa-glass fa-globe fa-group (alias) fa-hdd-o fa-headphones fa-heart fa-heart-o fa-home fa-inbox fa-info fa-info-circle fa-key fa-keyboard-o fa-laptop fa-leaf fa-legal (alias) fa-lemon-o fa-level-down fa-level-up fa-lightbulb-o fa-location-arrow fa-lock fa-magic fa-magnet fa-mail-forward (alias) fa-mail-reply (alias) fa-mail-reply-all fa-male fa-map-marker fa-meh-o fa-microphone fa-microphone-slash fa-minus fa-minus-circle fa-minus-square fa-minus-square-o fa-mobile fa-mobile-phone (alias) fa-money fa-moon-o fa-music fa-pencil fa-pencil-square fa-pencil-square-o fa-phone fa-phone-square fa-picture-o fa-plane fa-plus fa-plus-circle fa-plus-square
                        </f:boxContentWrap>
                    </f:boxWrap>
                </div>
                <div class="row">
                    <f:boxWrap boxColor="pink" columnLayoutClass="col-md-4">
                        <f:boxTitleAndToolWrap caption="Ga enak" icon="fa-magic" useClose="true" useCollapse="true"></f:boxTitleAndToolWrap>
                        <f:boxContentWrap className="weekday" useSlimScroll="true" >
                            ear (alias) fa-gears (alias) fa-gift fa-glass fa-globe fa-group (alias) fa-hdd-o fa-headphones fa-heart fa-heart-o fa-home fa-inbox fa-info fa-info-circle fa-key fa-keyboard-o fa-laptop fa-leaf fa-legal (alias) fa-lemon-o fa-level-down fa-level-up fa-lightbulb-o fa-location-arrow fa-lock fa-magic fa-magnet fa-mail-forward (alias) fa-mail-reply (alias) fa-mail-reply-all fa-male fa-map-marker fa-meh-o fa-microphone fa-microphone-slash fa-minus fa-minus-circle fa-minus-square fa-minus-square-o fa-mobile fa-mobile-phone (alias) fa-money fa-moon-o fa-music fa-pencil fa-pencil-square fa-pencil-square-o fa-phone fa-phone-square fa-picture-o fa-plane fa-plus fa-plus-circle fa-plus-square
                        </f:boxContentWrap>
                    </f:boxWrap>
                    <f:boxWrap boxColor="blue" columnLayoutClass="col-md-4">
                        <f:boxTitleAndToolWrap caption="Ga enak" icon="fa-magic" useClose="true" useCollapse="true"></f:boxTitleAndToolWrap>
                        <f:boxContentWrap className="weekday" useSlimScroll="true" >
                            ear (alias) fa-gears (alias) fa-gift fa-glass fa-globe fa-group (alias) fa-hdd-o fa-headphones fa-heart fa-heart-o fa-home fa-inbox fa-info fa-info-circle fa-key fa-keyboard-o fa-laptop fa-leaf fa-legal (alias) fa-lemon-o fa-level-down fa-level-up fa-lightbulb-o fa-location-arrow fa-lock fa-magic fa-magnet fa-mail-forward (alias) fa-mail-reply (alias) fa-mail-reply-all fa-male fa-map-marker fa-meh-o fa-microphone fa-microphone-slash fa-minus fa-minus-circle fa-minus-square fa-minus-square-o fa-mobile fa-mobile-phone (alias) fa-money fa-moon-o fa-music fa-pencil fa-pencil-square fa-pencil-square-o fa-phone fa-phone-square fa-picture-o fa-plane fa-plus fa-plus-circle fa-plus-square
                        </f:boxContentWrap>
                    </f:boxWrap>
                    <f:boxWrap boxColor="red" columnLayoutClass="col-md-4">
                        <f:boxTitleAndToolWrap caption="Ga enak" icon="fa-magic" useClose="true" useCollapse="true"></f:boxTitleAndToolWrap>
                        <f:boxContentWrap className="weekday" useSlimScroll="true" >
                            ear (alias) fa-gears (alias) fa-gift fa-glass fa-globe fa-group (alias) fa-hdd-o fa-headphones fa-heart fa-heart-o fa-home fa-inbox fa-info fa-info-circle fa-key fa-keyboard-o fa-laptop fa-leaf fa-legal (alias) fa-lemon-o fa-level-down fa-level-up fa-lightbulb-o fa-location-arrow fa-lock fa-magic fa-magnet fa-mail-forward (alias) fa-mail-reply (alias) fa-mail-reply-all fa-male fa-map-marker fa-meh-o fa-microphone fa-microphone-slash fa-minus fa-minus-circle fa-minus-square fa-minus-square-o fa-mobile fa-mobile-phone (alias) fa-money fa-moon-o fa-music fa-pencil fa-pencil-square fa-pencil-square-o fa-phone fa-phone-square fa-picture-o fa-plane fa-plus fa-plus-circle fa-plus-square
                        </f:boxContentWrap>
                    </f:boxWrap>
                </div>
                <div class="row">
                    <f:boxWrap boxColor="pink" columnLayoutClass="col-md-4">
                        <f:boxTitleAndToolWrap caption="Ga enak" icon="fa-magic" useClose="true" useCollapse="true"></f:boxTitleAndToolWrap>
                        <f:boxContentWrap className="weekday" useSlimScroll="true" >
                            ear (alias) fa-gears (alias) fa-gift fa-glass fa-globe fa-group (alias) fa-hdd-o fa-headphones fa-heart fa-heart-o fa-home fa-inbox fa-info fa-info-circle fa-key fa-keyboard-o fa-laptop fa-leaf fa-legal (alias) fa-lemon-o fa-level-down fa-level-up fa-lightbulb-o fa-location-arrow fa-lock fa-magic fa-magnet fa-mail-forward (alias) fa-mail-reply (alias) fa-mail-reply-all fa-male fa-map-marker fa-meh-o fa-microphone fa-microphone-slash fa-minus fa-minus-circle fa-minus-square fa-minus-square-o fa-mobile fa-mobile-phone (alias) fa-money fa-moon-o fa-music fa-pencil fa-pencil-square fa-pencil-square-o fa-phone fa-phone-square fa-picture-o fa-plane fa-plus fa-plus-circle fa-plus-square
                        </f:boxContentWrap>
                    </f:boxWrap>
                    <f:boxWrap boxColor="blue" columnLayoutClass="col-md-4">
                        <f:boxTitleAndToolWrap caption="Ga enak" icon="fa-magic" useClose="true" useCollapse="true"></f:boxTitleAndToolWrap>
                        <f:boxContentWrap className="weekday" useSlimScroll="true" >
                            ear (alias) fa-gears (alias) fa-gift fa-glass fa-globe fa-group (alias) fa-hdd-o fa-headphones fa-heart fa-heart-o fa-home fa-inbox fa-info fa-info-circle fa-key fa-keyboard-o fa-laptop fa-leaf fa-legal (alias) fa-lemon-o fa-level-down fa-level-up fa-lightbulb-o fa-location-arrow fa-lock fa-magic fa-magnet fa-mail-forward (alias) fa-mail-reply (alias) fa-mail-reply-all fa-male fa-map-marker fa-meh-o fa-microphone fa-microphone-slash fa-minus fa-minus-circle fa-minus-square fa-minus-square-o fa-mobile fa-mobile-phone (alias) fa-money fa-moon-o fa-music fa-pencil fa-pencil-square fa-pencil-square-o fa-phone fa-phone-square fa-picture-o fa-plane fa-plus fa-plus-circle fa-plus-square
                        </f:boxContentWrap>
                    </f:boxWrap>
                    <f:boxWrap boxColor="red" columnLayoutClass="col-md-4">
                        <f:boxTitleAndToolWrap caption="Ga enak" icon="fa-magic" useClose="true" useCollapse="true"></f:boxTitleAndToolWrap>
                        <f:boxContentWrap className="weekday" useSlimScroll="true" >
                            ear (alias) fa-gears (alias) fa-gift fa-glass fa-globe fa-group (alias) fa-hdd-o fa-headphones fa-heart fa-heart-o fa-home fa-inbox fa-info fa-info-circle fa-key fa-keyboard-o fa-laptop fa-leaf fa-legal (alias) fa-lemon-o fa-level-down fa-level-up fa-lightbulb-o fa-location-arrow fa-lock fa-magic fa-magnet fa-mail-forward (alias) fa-mail-reply (alias) fa-mail-reply-all fa-male fa-map-marker fa-meh-o fa-microphone fa-microphone-slash fa-minus fa-minus-circle fa-minus-square fa-minus-square-o fa-mobile fa-mobile-phone (alias) fa-money fa-moon-o fa-music fa-pencil fa-pencil-square fa-pencil-square-o fa-phone fa-phone-square fa-picture-o fa-plane fa-plus fa-plus-circle fa-plus-square
                        </f:boxContentWrap>
                    </f:boxWrap>
                </div>
                       
             
                        
                    <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#" style="display: none;"><i class="fa fa-chevron-up"></i></a>
                </div>
                
                
                
                <!-- END Content -->
            </div>
  
      
        
        <script type="text/javascript" src="<s:url value="/bundles/whitelabel.js"/>"></script>               
        <!-- 
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="http://themes.shamsoft.net/flaty/assets/bootstrap/js/bootstrap.min.js"></script>
          <script src="http://themes.shamsoft.net/flaty/assets/jquery-slimscroll/jquery.slimscroll.min.js"></script>
          <script src="http://themes.shamsoft.net/flaty/assets/jquery-cookie/jquery.cookie.js"></script>
          <script src="http://themes.shamsoft.net/flaty/js/flaty.js"></script>
          <script src="http://themes.shamsoft.net/flaty/js/flaty-demo-codes.js"></script>
        -->  
    </body>
</html>
