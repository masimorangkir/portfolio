<%-- 
    Document   : dashboard_production
    Created on : Aug 19, 2014, 5:08:00 PM
    Author     : Genesys8
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="row">
    <div class="col-md-6">

        <div class="widget">
            <div class="widget-header bg-blueberry">
                <span class="widget-caption" id="title">Top 10 Report By Value (Input)</span>
                <div class="widget-buttons">
                    <a href="#" data-toggle="maximize">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a href="#" data-toggle="collapse">
                        <i class="fa fa-minus"></i>
                    </a>
                    <a href="#" data-toggle="dispose">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="widget-body">
                <div class="btn-group period" data-param="in">
                    <!--<a href="#" class="btn btn-primary submit"><i class="fa fa-search"></i> Search</a>-->
                    <div id="top10inPrice" class="control-range btn btn-primary price" data-opens="right">
                        <i class="fa fa-calendar fa-lg"></i>
                        <span>August 1, 2014 - August 31, 2014</span> <b class="caret" style="position: relative; top: -5px"></b>
                    </div>
                    <button type="button" class="btn btn-default btn-success price" data-period="d"><i class="fa fa-bar-chart-o"></i> Chart</button>
                    <%--<button type="button" class="btn btn-default price" data-period="m"><i class="fa fa-table"></i> List</button>--%>
                </div>
                <div id="containerTop10InPrice" class="map_reflow" style="width:100%; height:300px;"></div>
                <!--<div id="horizonal-chart" class="chart chart-lg" style="padding: 0px; position: relative;"><canvas class="flot-base" width="732" height="250" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 732px; height: 250px;"></canvas><div class="flot-text" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);"><div class="flot-x-axis flot-x1-axis xAxis x1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;"><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 55px; text-align: center;">0.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 137px; text-align: center;">1.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 220px; text-align: center;">2.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 303px; text-align: center;">3.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 386px; text-align: center;">4.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 468px; text-align: center;">5.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 551px; text-align: center;">6.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 634px; text-align: center;">7.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 717px; text-align: center;">8.0</div></div><div class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;"><div class="flot-tick-label tickLabel" style="position: absolute; top: 223px; left: 40px; text-align: right;">Yes</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 174px; left: 42px; text-align: right;">No</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 126px; left: 22px; text-align: right;">Maybe</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 77px; left: 0px; text-align: right;">Sometimes</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 29px; left: 27px; text-align: right;">Never</div></div></div><canvas class="flot-overlay" width="732" height="250" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 732px; height: 250px;"></canvas></div>-->

            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="widget">
            <div class="widget-header bg-blueberry">
                <span class="widget-caption" id="title">Top 10 Report By Value (Output)</span>
                <div class="widget-buttons">
                    <a href="#" data-toggle="maximize">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a href="#" data-toggle="collapse">
                        <i class="fa fa-minus"></i>
                    </a>
                    <a href="#" data-toggle="dispose">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="widget-body">
                <div class="btn-group period" data-param="out">
                    <!--<a href="#" class="btn btn-primary submit"><i class="fa fa-search"></i> Search</a>-->
                    <div id="top10outPrice" class="control-range btn btn-primary price" data-opens="left">
                        <i class="fa fa-calendar fa-lg"></i>
                        <span>August 1, 2014 - August 31, 2014</span> <b class="caret" style="position: relative; top: -5px"></b>
                    </div>
                    <button type="button" class="btn btn-default btn-success price" data-period="d"><i class="fa fa-bar-chart-o"></i> Chart</button>
                    <%--<button type="button" class="btn btn-default price" data-period="m"><i class="fa fa-table"></i> List</button>--%>
                </div>
                <div id="containerTop10OutPrice" class="map_reflow" style="width:100%; height:300px;"></div>
                <!--<div id="horizonal-chart" class="chart chart-lg" style="padding: 0px; position: relative;"><canvas class="flot-base" width="732" height="250" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 732px; height: 250px;"></canvas><div class="flot-text" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);"><div class="flot-x-axis flot-x1-axis xAxis x1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;"><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 55px; text-align: center;">0.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 137px; text-align: center;">1.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 220px; text-align: center;">2.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 303px; text-align: center;">3.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 386px; text-align: center;">4.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 468px; text-align: center;">5.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 551px; text-align: center;">6.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 634px; text-align: center;">7.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 717px; text-align: center;">8.0</div></div><div class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;"><div class="flot-tick-label tickLabel" style="position: absolute; top: 223px; left: 40px; text-align: right;">Yes</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 174px; left: 42px; text-align: right;">No</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 126px; left: 22px; text-align: right;">Maybe</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 77px; left: 0px; text-align: right;">Sometimes</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 29px; left: 27px; text-align: right;">Never</div></div></div><canvas class="flot-overlay" width="732" height="250" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 732px; height: 250px;"></canvas></div>-->

            </div>
        </div>
    </div>
</div>



<div class="row">
    <div class="col-md-6">

        <div class="widget">
            <div class="widget-header bg-blueberry">
                <span class="widget-caption" id="title">Top 10 Report By Quantity (Input)</span>
                <div class="widget-buttons">
                    <a href="#" data-toggle="maximize">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a href="#" data-toggle="collapse">
                        <i class="fa fa-minus"></i>
                    </a>
                    <a href="#" data-toggle="dispose">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="widget-body">
                <div class="btn-group period" data-param="in">
                    <!--<a href="#" class="btn btn-primary submit"><i class="fa fa-search"></i> Search</a>-->
                    <div id="top10in" class="control-range btn btn-primary top10" data-opens="right">
                        <i class="fa fa-calendar fa-lg"></i>
                        <span>August 1, 2014 - August 31, 2014</span> <b class="caret" style="position: relative; top: -5px"></b>
                    </div>
                    <button type="button" class="btn btn-default btn-success top10" data-period="d"><i class="fa fa-bar-chart-o"></i> Chart</button>
                    <%--<button type="button" class="btn btn-default top10" data-period="m"><i class="fa fa-table"></i> List</button>--%>
                </div>
                <div id="containerTop10In" class="map_reflow" style="width:100%; height:300px;"></div>
                <!--<div id="horizonal-chart" class="chart chart-lg" style="padding: 0px; position: relative;"><canvas class="flot-base" width="732" height="250" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 732px; height: 250px;"></canvas><div class="flot-text" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);"><div class="flot-x-axis flot-x1-axis xAxis x1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;"><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 55px; text-align: center;">0.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 137px; text-align: center;">1.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 220px; text-align: center;">2.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 303px; text-align: center;">3.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 386px; text-align: center;">4.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 468px; text-align: center;">5.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 551px; text-align: center;">6.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 634px; text-align: center;">7.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 717px; text-align: center;">8.0</div></div><div class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;"><div class="flot-tick-label tickLabel" style="position: absolute; top: 223px; left: 40px; text-align: right;">Yes</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 174px; left: 42px; text-align: right;">No</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 126px; left: 22px; text-align: right;">Maybe</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 77px; left: 0px; text-align: right;">Sometimes</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 29px; left: 27px; text-align: right;">Never</div></div></div><canvas class="flot-overlay" width="732" height="250" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 732px; height: 250px;"></canvas></div>-->

            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="widget">
            <div class="widget-header bg-blueberry">
                <span class="widget-caption" id="title">Top 10 Report By Quantity (Output)</span>
                <div class="widget-buttons">
                    <a href="#" data-toggle="maximize">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a href="#" data-toggle="collapse">
                        <i class="fa fa-minus"></i>
                    </a>
                    <a href="#" data-toggle="dispose">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="widget-body">
                <div class="btn-group period" data-param="out">
                    <!--<a href="#" class="btn btn-primary submit"><i class="fa fa-search"></i> Search</a>-->
                    <div id="top10out" class="control-range btn btn-primary top10" data-opens="left">
                        <i class="fa fa-calendar fa-lg"></i>
                        <span>August 1, 2014 - August 31, 2014</span> <b class="caret" style="position: relative; top: -5px"></b>
                    </div>
                    <button type="button" class="btn btn-default btn-success top10" data-period="d"><i class="fa fa-bar-chart-o"></i> Chart</button>
                    <%--<button type="button" class="btn btn-default top10" data-period="m"><i class="fa fa-table"></i> List</button>--%>
                </div>
                <div id="containerTop10Out" class="map_reflow" style="width:100%; height:300px;"></div>
                <!--<div id="horizonal-chart" class="chart chart-lg" style="padding: 0px; position: relative;"><canvas class="flot-base" width="732" height="250" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 732px; height: 250px;"></canvas><div class="flot-text" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);"><div class="flot-x-axis flot-x1-axis xAxis x1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;"><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 55px; text-align: center;">0.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 137px; text-align: center;">1.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 220px; text-align: center;">2.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 303px; text-align: center;">3.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 386px; text-align: center;">4.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 468px; text-align: center;">5.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 551px; text-align: center;">6.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 634px; text-align: center;">7.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 717px; text-align: center;">8.0</div></div><div class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;"><div class="flot-tick-label tickLabel" style="position: absolute; top: 223px; left: 40px; text-align: right;">Yes</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 174px; left: 42px; text-align: right;">No</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 126px; left: 22px; text-align: right;">Maybe</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 77px; left: 0px; text-align: right;">Sometimes</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 29px; left: 27px; text-align: right;">Never</div></div></div><canvas class="flot-overlay" width="732" height="250" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 732px; height: 250px;"></canvas></div>-->

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">

        <div class="widget">
            <div class="widget-header bg-blueberry">
                <span class="widget-caption" id="title">Accumulative Qty (Input)</span>
                <div class="widget-buttons">
                    <a href="#" data-toggle="maximize">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a href="#" data-toggle="collapse">
                        <i class="fa fa-minus"></i>
                    </a>
                    <a href="#" data-toggle="dispose">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="widget-body">
                <div class="btn-group period" data-param="in">
                    <!--<a href="#" class="btn btn-primary submit"><i class="fa fa-search"></i> Search</a>-->
                    <div id="reportrange" class="control-range btn btn-primary" data-opens="right">
                        <i class="fa fa-calendar fa-lg"></i>
                        <span>August 1, 2014 - August 31, 2014</span> <b class="caret" style="position: relative; top: -5px"></b>
                    </div>
                    <button type="button" class="btn btn-default btn-success" data-period="d"><i class="fa fa-clock-o"></i> Day</button>
                    <button type="button" class="btn btn-default" data-period="m"><i class="fa fa-moon-o"></i> Month</button>
                    <button type="button" class="btn btn-default" data-period="y"><i class="fa fa-calendar"></i> Year</button>
                </div>
                <div id="containerIn" class="map_reflow" style="width:100%; height:300px;"></div>
                <!--<div id="horizonal-chart" class="chart chart-lg" style="padding: 0px; position: relative;"><canvas class="flot-base" width="732" height="250" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 732px; height: 250px;"></canvas><div class="flot-text" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);"><div class="flot-x-axis flot-x1-axis xAxis x1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;"><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 55px; text-align: center;">0.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 137px; text-align: center;">1.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 220px; text-align: center;">2.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 303px; text-align: center;">3.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 386px; text-align: center;">4.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 468px; text-align: center;">5.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 551px; text-align: center;">6.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 634px; text-align: center;">7.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 717px; text-align: center;">8.0</div></div><div class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;"><div class="flot-tick-label tickLabel" style="position: absolute; top: 223px; left: 40px; text-align: right;">Yes</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 174px; left: 42px; text-align: right;">No</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 126px; left: 22px; text-align: right;">Maybe</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 77px; left: 0px; text-align: right;">Sometimes</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 29px; left: 27px; text-align: right;">Never</div></div></div><canvas class="flot-overlay" width="732" height="250" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 732px; height: 250px;"></canvas></div>-->

            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="widget">
            <div class="widget-header bg-blueberry">
                <span class="widget-caption" id="title">Accumulative Qty (Output)</span>
                <div class="widget-buttons">
                    <a href="#" data-toggle="maximize">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a href="#" data-toggle="collapse">
                        <i class="fa fa-minus"></i>
                    </a>
                    <a href="#" data-toggle="dispose">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="widget-body">
                <div class="btn-group period" data-param="out">
                    <!--<a href="#" class="btn btn-primary submit"><i class="fa fa-search"></i> Search</a>-->
                    <div id="reportrange2" class="control-range btn btn-primary" data-opens="left">
                        <i class="fa fa-calendar fa-lg"></i>
                        <span>August 1, 2014 - August 31, 2014</span> <b class="caret" style="position: relative; top: -5px"></b>
                    </div>
                    <button type="button" class="btn btn-default btn-success" data-period="d"><i class="fa fa-clock-o"></i> Day</button>
                    <button type="button" class="btn btn-default" data-period="m"><i class="fa fa-moon-o"></i> Month</button>
                    <button type="button" class="btn btn-default" data-period="y"><i class="fa fa-calendar"></i> Year</button>
                </div>
                <div id="containerOut" class="map_reflow" style="width:100%; height:300px;"></div>
                <!--<div id="horizonal-chart" class="chart chart-lg" style="padding: 0px; position: relative;"><canvas class="flot-base" width="732" height="250" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 732px; height: 250px;"></canvas><div class="flot-text" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);"><div class="flot-x-axis flot-x1-axis xAxis x1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;"><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 55px; text-align: center;">0.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 137px; text-align: center;">1.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 220px; text-align: center;">2.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 303px; text-align: center;">3.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 386px; text-align: center;">4.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 468px; text-align: center;">5.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 551px; text-align: center;">6.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 634px; text-align: center;">7.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 235px; left: 717px; text-align: center;">8.0</div></div><div class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;"><div class="flot-tick-label tickLabel" style="position: absolute; top: 223px; left: 40px; text-align: right;">Yes</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 174px; left: 42px; text-align: right;">No</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 126px; left: 22px; text-align: right;">Maybe</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 77px; left: 0px; text-align: right;">Sometimes</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 29px; left: 27px; text-align: right;">Never</div></div></div><canvas class="flot-overlay" width="732" height="250" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 732px; height: 250px;"></canvas></div>-->

            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="widget">
            <div class="widget-header bg-blueberry">
                <div class="widget-caption" id="mySearch">Input Output Comparison</div>
                <div class="widget-buttons">
                    <a href="#" data-toggle="maximize">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a href="#" data-toggle="collapse">
                        <i class="fa fa-minus"></i>
                    </a>
                    <a href="#" data-toggle="dispose">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="widget-body">

                <div class="col-md-3">
                    <div class="form-group">
                        <label for="paramStart">Range Date</label>
                        <div id="periodRange" class="control-range btn btn-primary no-map" style="width:100%" data-opens="right">
                            <i class="fa fa-calendar fa-lg"></i>
                            <span>August 1, 2014 - August 31, 2014</span> <b class="caret" style="position: relative; top: -5px"></b>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label for="paramPiid">Processing Instruction</label>
                        <input type="text" class="form-control control-search2" id="paramPiid" name="paramPiid" placeholder="" data-tips="Search PI..." data-link="pi">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="paramSoid">Sales Order</label>
                        <input type="text" class="form-control control-search2" id="paramSoid" name="paramSoid" placeholder="" data-tips="Search SO..." data-link="so">
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                        <label for="paramCustomerName">Customer Name</label>
                        <input type="text" class="form-control control-search2" id="paramCustomerName" name="paramCustomerName" placeholder="" data-tips="Search Customer..." data-link="customer">
                    </div>

                </div>
                <div class="form-group">
                    <label for="paramCustomerName"></label>
                    <div><a id="submitComparison" class="btn btn-primary"><i class="fa fa-search"></i> Submit</a></div>
                </div>

                <hr class="wide">

                <div class="table-scrollable">
                    <table id="tbl_compare" class="table table-striped table-bordered table-hover" style="width:100%">
                        <thead>
                            <tr>
                                <th scope="col" rowspan="2" >PI Date</th>
                                <th scope="col" rowspan="2" >SO ID</th>
                                <th scope="col" rowspan="2" >Customer Name</th>
                                <th scope="col" rowspan="2" >PI ID</th>
                                <th scope="col" rowspan="1" colspan="1" >Input</th>
                                <th scope="col" rowspan="1" colspan="5" >Output</th>
                            </tr>
                            <tr>
                                <th scope="col" >Value (IDR)</th>
                                <th scope="col" >Value (IDR)</th>
                                <th scope="col" >Qty</th>
                                <th scope="col" >UoM</th>
                                <th scope="col" >Inventory ID</th>
                                <th scope="col" >Inventory Type</th>
                            </tr>
                        </thead>
                    </table>

                </div>
            </div>
        </div>  
    </div>
</div>