<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c2" uri="http://java.sun.com/jstl/core_rt" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>

<div class="row">
    <div class="col-lg-6 col-sm-6 col-xs-12">
        <div class="widget radius-bordered">
            <div class="widget-header bg-blue">
                <span class="widget-caption">Profile Information</span>
            </div>
            <div class="widget-body">
                <div id="registration-form">
                    <form role="form" action="<s:url value="/profile/save"/>" method="post">
                        <div class="form-title">
                            User Information
                        </div>
                        <div class="form-group">
                            <span class="input-icon icon-right">
                                <label for="user">Username : <strong><c2:out value="${credential.user}"/></strong></label>
                            </span>
                        </div>
                        <div class="form-group">
                            <span class="input-icon icon-right">
                                <label for="email">Email Address</label>
                                <input type="text" class="form-control" id="email" name="email" placeholder="Email Address" value="<c2:out value="${credential.email}"/>">
                                <i class="fa fa-envelope-o circular"></i>
                            </span>
                        </div>
                        <div class="form-group">
                            <span class="input-icon icon-right">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" id="password" name="password" placeholder="Password" value="<c2:out value="${credential.password}"/>">
                                <i class="fa fa-lock circular"></i>
                            </span>
                        </div>
                        <div class="form-group">
                            <span class="input-icon icon-right">
                                <label for="confirm">Confirm Password</label>
                                <input type="password" class="form-control" id="confirm" name="confirm" placeholder="Confirm Password" value="<c2:out value="${credential.password}"/>">
                                <i class="fa fa-lock circular"></i>
                            </span>
                        </div>
                        <div class="form-title">
                            Personal Information
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <span class="input-icon icon-right">
                                        <label for="name">Real Name</label>
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<c2:out value="${credential.name}"/>">
                                        <i class="fa fa-user"></i>
                                    </span>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <span class="input-icon icon-right">
                                        <label for="phone">Phone No.</label>
                                        <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone" value="<c2:out value="${credential.phone}"/>">
                                        <i class="glyphicon glyphicon-earphone"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <span class="input-icon icon-right">
                                        <label for="mobile">Mobile No.</label>
                                        <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile" value="<c2:out value="${credential.mobile}"/>">
                                        <i class="glyphicon glyphicon-phone"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <hr class="wide">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <span class="input-icon icon-right">
                                        <label for="birthDate">Birth Date</label>
                                        <input class="form-control date-picker" id="id-date-picker-1" id="newDate" name="newDate" value="<c2:out value="${credential.birthDate}"/>" type="text" data-date-format="yyyy-mm-dd" placeholder="Birth Date">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <span class="input-icon icon-right">
                                        <label for="birthPlace">Birth Place</label>
                                        <input type="text" class="form-control" id="birthPlace" name="birthPlace" placeholder="Birth Place" value="<c2:out value="${credential.birthPlace}"/>">
                                        <i class="fa fa-globe"></i>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-blue">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
