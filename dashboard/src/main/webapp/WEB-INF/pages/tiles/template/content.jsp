<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="f" uri="/WEB-INF/tld/dashboard.tld" %>

<div class="row">          
    <f:boxWrap boxColor="black" columnLayoutClass="col-md-5">
        <f:boxTitleAndToolWrap caption="Purchase Order" icon="fa-bolt" useClose="true" useCollapse="true">

        </f:boxTitleAndToolWrap> 
        <f:boxContentWrap className="weekly-stats" useListStyle="true" useSlimScroll="true">
            <li>
                <span class="inline-sparkline"><canvas width="70" height="26" style="display: inline-block; width: 70px; height: 26px; vertical-align: top;"></canvas></span>
                Visits: <span class="value">376</span>
            </li>
            <li>
                <span class="inline-sparkline"><canvas width="70" height="26" style="display: inline-block; width: 70px; height: 26px; vertical-align: top;"></canvas></span>
                Unique Visitors: <span class="value">238</span>
            </li>
            <li>
                <span class="inline-sparkline"><canvas width="70" height="26" style="display: inline-block; width: 70px; height: 26px; vertical-align: top;"></canvas></span>
                Page Views: <span class="value">514</span>
            </li>
            <li>
                <span class="inline-sparkline"><canvas width="70" height="26" style="display: inline-block; width: 70px; height: 26px; vertical-align: top;"></canvas></span>
                Pages / Visit: <span class="value">1.43</span>
            </li>
            <li>
                <span class="inline-sparkline"><canvas width="70" height="26" style="display: inline-block; width: 70px; height: 26px; vertical-align: top;"></canvas></span>
                Avg. Visit Time: <span class="value">00:02:34</span>
            </li>
            <li>
                <span class="inline-sparkline"><canvas width="70" height="26" style="display: inline-block; width: 70px; height: 26px; vertical-align: top;"></canvas></span>
                Bounce Rate: <span class="value">73.56%</span>
            </li>
            <li>
                <span class="inline-sparkline"><canvas width="70" height="26" style="display: inline-block; width: 70px; height: 26px; vertical-align: top;"></canvas></span>
                % New Visits: <span class="value">82.65%</span>
            </li>
        </f:boxContentWrap>
    </f:boxWrap>
    <f:boxWrap boxColor="magenta" columnLayoutClass="col-md-7">
        <f:boxTitleAndToolWrap caption="GIla" icon="fa-bolt" useClose="true"></f:boxTitleAndToolWrap>
        <f:boxContentWrap className="tile" >
            Wkwkwk asd;fkajsd;fkja s;dfk ja;sd f;d j;fldlajksd;a jd;flsda fja; fja; ask;fldjka sd; fjas; fjd;sfjka sfja;ldf
            asdfaskdjf; ;akjsd;fjk asfjsa;jk as; jkas; fjsd
        </f:boxContentWrap>
    </f:boxWrap>

</div>
<div class="row">
    <f:boxWrap boxColor="pink" columnLayoutClass="col-md-4">
        <f:boxTitleAndToolWrap caption="Ga enak" icon="fa-magic" useClose="true" useCollapse="true"></f:boxTitleAndToolWrap>
        <f:boxContentWrap className="weekday" useSlimScroll="true" >
            ear (alias) fa-gears (alias) fa-gift fa-glass fa-globe fa-group (alias) fa-hdd-o fa-headphones fa-heart fa-heart-o fa-home fa-inbox fa-info fa-info-circle fa-key fa-keyboard-o fa-laptop fa-leaf fa-legal (alias) fa-lemon-o fa-level-down fa-level-up fa-lightbulb-o fa-location-arrow fa-lock fa-magic fa-magnet fa-mail-forward (alias) fa-mail-reply (alias) fa-mail-reply-all fa-male fa-map-marker fa-meh-o fa-microphone fa-microphone-slash fa-minus fa-minus-circle fa-minus-square fa-minus-square-o fa-mobile fa-mobile-phone (alias) fa-money fa-moon-o fa-music fa-pencil fa-pencil-square fa-pencil-square-o fa-phone fa-phone-square fa-picture-o fa-plane fa-plus fa-plus-circle fa-plus-square
        </f:boxContentWrap>
    </f:boxWrap>
    <f:boxWrap boxColor="blue" columnLayoutClass="col-md-4">
        <f:boxTitleAndToolWrap caption="Ga enak" icon="fa-magic" useClose="true" useCollapse="true"></f:boxTitleAndToolWrap>
        <f:boxContentWrap className="weekday" useSlimScroll="true" >
            ear (alias) fa-gears (alias) fa-gift fa-glass fa-globe fa-group (alias) fa-hdd-o fa-headphones fa-heart fa-heart-o fa-home fa-inbox fa-info fa-info-circle fa-key fa-keyboard-o fa-laptop fa-leaf fa-legal (alias) fa-lemon-o fa-level-down fa-level-up fa-lightbulb-o fa-location-arrow fa-lock fa-magic fa-magnet fa-mail-forward (alias) fa-mail-reply (alias) fa-mail-reply-all fa-male fa-map-marker fa-meh-o fa-microphone fa-microphone-slash fa-minus fa-minus-circle fa-minus-square fa-minus-square-o fa-mobile fa-mobile-phone (alias) fa-money fa-moon-o fa-music fa-pencil fa-pencil-square fa-pencil-square-o fa-phone fa-phone-square fa-picture-o fa-plane fa-plus fa-plus-circle fa-plus-square
        </f:boxContentWrap>
    </f:boxWrap>
    <f:boxWrap boxColor="red" columnLayoutClass="col-md-4">
        <f:boxTitleAndToolWrap caption="Ga enak" icon="fa-magic" useClose="true" useCollapse="true"></f:boxTitleAndToolWrap>
        <f:boxContentWrap className="weekday" useSlimScroll="true" >
            ear (alias) fa-gears (alias) fa-gift fa-glass fa-globe fa-group (alias) fa-hdd-o fa-headphones fa-heart fa-heart-o fa-home fa-inbox fa-info fa-info-circle fa-key fa-keyboard-o fa-laptop fa-leaf fa-legal (alias) fa-lemon-o fa-level-down fa-level-up fa-lightbulb-o fa-location-arrow fa-lock fa-magic fa-magnet fa-mail-forward (alias) fa-mail-reply (alias) fa-mail-reply-all fa-male fa-map-marker fa-meh-o fa-microphone fa-microphone-slash fa-minus fa-minus-circle fa-minus-square fa-minus-square-o fa-mobile fa-mobile-phone (alias) fa-money fa-moon-o fa-music fa-pencil fa-pencil-square fa-pencil-square-o fa-phone fa-phone-square fa-picture-o fa-plane fa-plus fa-plus-circle fa-plus-square
        </f:boxContentWrap>
    </f:boxWrap>
</div>
<div class="row">
    <f:boxWrap boxColor="pink" columnLayoutClass="col-md-4">
        <f:boxTitleAndToolWrap caption="Ga enak" icon="fa-magic" useClose="true" useCollapse="true"></f:boxTitleAndToolWrap>
        <f:boxContentWrap className="weekday" useSlimScroll="true" >
            ear (alias) fa-gears (alias) fa-gift fa-glass fa-globe fa-group (alias) fa-hdd-o fa-headphones fa-heart fa-heart-o fa-home fa-inbox fa-info fa-info-circle fa-key fa-keyboard-o fa-laptop fa-leaf fa-legal (alias) fa-lemon-o fa-level-down fa-level-up fa-lightbulb-o fa-location-arrow fa-lock fa-magic fa-magnet fa-mail-forward (alias) fa-mail-reply (alias) fa-mail-reply-all fa-male fa-map-marker fa-meh-o fa-microphone fa-microphone-slash fa-minus fa-minus-circle fa-minus-square fa-minus-square-o fa-mobile fa-mobile-phone (alias) fa-money fa-moon-o fa-music fa-pencil fa-pencil-square fa-pencil-square-o fa-phone fa-phone-square fa-picture-o fa-plane fa-plus fa-plus-circle fa-plus-square
        </f:boxContentWrap>
    </f:boxWrap>
    <f:boxWrap boxColor="blue" columnLayoutClass="col-md-4">
        <f:boxTitleAndToolWrap caption="Ga enak" icon="fa-magic" useClose="true" useCollapse="true"></f:boxTitleAndToolWrap>
        <f:boxContentWrap className="weekday" useSlimScroll="true" >
            ear (alias) fa-gears (alias) fa-gift fa-glass fa-globe fa-group (alias) fa-hdd-o fa-headphones fa-heart fa-heart-o fa-home fa-inbox fa-info fa-info-circle fa-key fa-keyboard-o fa-laptop fa-leaf fa-legal (alias) fa-lemon-o fa-level-down fa-level-up fa-lightbulb-o fa-location-arrow fa-lock fa-magic fa-magnet fa-mail-forward (alias) fa-mail-reply (alias) fa-mail-reply-all fa-male fa-map-marker fa-meh-o fa-microphone fa-microphone-slash fa-minus fa-minus-circle fa-minus-square fa-minus-square-o fa-mobile fa-mobile-phone (alias) fa-money fa-moon-o fa-music fa-pencil fa-pencil-square fa-pencil-square-o fa-phone fa-phone-square fa-picture-o fa-plane fa-plus fa-plus-circle fa-plus-square
        </f:boxContentWrap>
    </f:boxWrap>
    <f:boxWrap boxColor="red" columnLayoutClass="col-md-4">
        <f:boxTitleAndToolWrap caption="Ga enak" icon="fa-magic" useClose="true" useCollapse="true"></f:boxTitleAndToolWrap>
        <f:boxContentWrap className="weekday" useSlimScroll="true" >
            ear (alias) fa-gears (alias) fa-gift fa-glass fa-globe fa-group (alias) fa-hdd-o fa-headphones fa-heart fa-heart-o fa-home fa-inbox fa-info fa-info-circle fa-key fa-keyboard-o fa-laptop fa-leaf fa-legal (alias) fa-lemon-o fa-level-down fa-level-up fa-lightbulb-o fa-location-arrow fa-lock fa-magic fa-magnet fa-mail-forward (alias) fa-mail-reply (alias) fa-mail-reply-all fa-male fa-map-marker fa-meh-o fa-microphone fa-microphone-slash fa-minus fa-minus-circle fa-minus-square fa-minus-square-o fa-mobile fa-mobile-phone (alias) fa-money fa-moon-o fa-music fa-pencil fa-pencil-square fa-pencil-square-o fa-phone fa-phone-square fa-picture-o fa-plane fa-plus fa-plus-circle fa-plus-square
        </f:boxContentWrap>
    </f:boxWrap>
</div>
<div class="row">
    <f:boxWrap boxColor="pink" columnLayoutClass="col-md-4">
        <f:boxTitleAndToolWrap caption="Ga enak" icon="fa-magic" useClose="true" useCollapse="true"></f:boxTitleAndToolWrap>
        <f:boxContentWrap className="weekday" useSlimScroll="true" >
            ear (alias) fa-gears (alias) fa-gift fa-glass fa-globe fa-group (alias) fa-hdd-o fa-headphones fa-heart fa-heart-o fa-home fa-inbox fa-info fa-info-circle fa-key fa-keyboard-o fa-laptop fa-leaf fa-legal (alias) fa-lemon-o fa-level-down fa-level-up fa-lightbulb-o fa-location-arrow fa-lock fa-magic fa-magnet fa-mail-forward (alias) fa-mail-reply (alias) fa-mail-reply-all fa-male fa-map-marker fa-meh-o fa-microphone fa-microphone-slash fa-minus fa-minus-circle fa-minus-square fa-minus-square-o fa-mobile fa-mobile-phone (alias) fa-money fa-moon-o fa-music fa-pencil fa-pencil-square fa-pencil-square-o fa-phone fa-phone-square fa-picture-o fa-plane fa-plus fa-plus-circle fa-plus-square
        </f:boxContentWrap>
    </f:boxWrap>
    <f:boxWrap boxColor="blue" columnLayoutClass="col-md-4">
        <f:boxTitleAndToolWrap caption="Ga enak" icon="fa-magic" useClose="true" useCollapse="true"></f:boxTitleAndToolWrap>
        <f:boxContentWrap className="weekday" useSlimScroll="true" >
            ear (alias) fa-gears (alias) fa-gift fa-glass fa-globe fa-group (alias) fa-hdd-o fa-headphones fa-heart fa-heart-o fa-home fa-inbox fa-info fa-info-circle fa-key fa-keyboard-o fa-laptop fa-leaf fa-legal (alias) fa-lemon-o fa-level-down fa-level-up fa-lightbulb-o fa-location-arrow fa-lock fa-magic fa-magnet fa-mail-forward (alias) fa-mail-reply (alias) fa-mail-reply-all fa-male fa-map-marker fa-meh-o fa-microphone fa-microphone-slash fa-minus fa-minus-circle fa-minus-square fa-minus-square-o fa-mobile fa-mobile-phone (alias) fa-money fa-moon-o fa-music fa-pencil fa-pencil-square fa-pencil-square-o fa-phone fa-phone-square fa-picture-o fa-plane fa-plus fa-plus-circle fa-plus-square
        </f:boxContentWrap>
    </f:boxWrap>
    <f:boxWrap boxColor="red" columnLayoutClass="col-md-4">
        <f:boxTitleAndToolWrap caption="Ga enak" icon="fa-magic" useClose="true" useCollapse="true"></f:boxTitleAndToolWrap>
        <f:boxContentWrap className="weekday" useSlimScroll="true" >
            ear (alias) fa-gears (alias) fa-gift fa-glass fa-globe fa-group (alias) fa-hdd-o fa-headphones fa-heart fa-heart-o fa-home fa-inbox fa-info fa-info-circle fa-key fa-keyboard-o fa-laptop fa-leaf fa-legal (alias) fa-lemon-o fa-level-down fa-level-up fa-lightbulb-o fa-location-arrow fa-lock fa-magic fa-magnet fa-mail-forward (alias) fa-mail-reply (alias) fa-mail-reply-all fa-male fa-map-marker fa-meh-o fa-microphone fa-microphone-slash fa-minus fa-minus-circle fa-minus-square fa-minus-square-o fa-mobile fa-mobile-phone (alias) fa-money fa-moon-o fa-music fa-pencil fa-pencil-square fa-pencil-square-o fa-phone fa-phone-square fa-picture-o fa-plane fa-plus fa-plus-circle fa-plus-square
        </f:boxContentWrap>
    </f:boxWrap>
</div>
<div class="row">
    <f:boxWrap boxColor="pink" columnLayoutClass="col-md-4">
        <f:boxTitleAndToolWrap caption="Ga enak" icon="fa-magic" useClose="true" useCollapse="true"></f:boxTitleAndToolWrap>
        <f:boxContentWrap className="weekday" useSlimScroll="true" >
            ear (alias) fa-gears (alias) fa-gift fa-glass fa-globe fa-group (alias) fa-hdd-o fa-headphones fa-heart fa-heart-o fa-home fa-inbox fa-info fa-info-circle fa-key fa-keyboard-o fa-laptop fa-leaf fa-legal (alias) fa-lemon-o fa-level-down fa-level-up fa-lightbulb-o fa-location-arrow fa-lock fa-magic fa-magnet fa-mail-forward (alias) fa-mail-reply (alias) fa-mail-reply-all fa-male fa-map-marker fa-meh-o fa-microphone fa-microphone-slash fa-minus fa-minus-circle fa-minus-square fa-minus-square-o fa-mobile fa-mobile-phone (alias) fa-money fa-moon-o fa-music fa-pencil fa-pencil-square fa-pencil-square-o fa-phone fa-phone-square fa-picture-o fa-plane fa-plus fa-plus-circle fa-plus-square
        </f:boxContentWrap>
    </f:boxWrap>
    <f:boxWrap boxColor="blue" columnLayoutClass="col-md-4">
        <f:boxTitleAndToolWrap caption="Ga enak" icon="fa-magic" useClose="true" useCollapse="true"></f:boxTitleAndToolWrap>
        <f:boxContentWrap className="weekday" useSlimScroll="true" >
            ear (alias) fa-gears (alias) fa-gift fa-glass fa-globe fa-group (alias) fa-hdd-o fa-headphones fa-heart fa-heart-o fa-home fa-inbox fa-info fa-info-circle fa-key fa-keyboard-o fa-laptop fa-leaf fa-legal (alias) fa-lemon-o fa-level-down fa-level-up fa-lightbulb-o fa-location-arrow fa-lock fa-magic fa-magnet fa-mail-forward (alias) fa-mail-reply (alias) fa-mail-reply-all fa-male fa-map-marker fa-meh-o fa-microphone fa-microphone-slash fa-minus fa-minus-circle fa-minus-square fa-minus-square-o fa-mobile fa-mobile-phone (alias) fa-money fa-moon-o fa-music fa-pencil fa-pencil-square fa-pencil-square-o fa-phone fa-phone-square fa-picture-o fa-plane fa-plus fa-plus-circle fa-plus-square
        </f:boxContentWrap>
    </f:boxWrap>
    <f:boxWrap boxColor="red" columnLayoutClass="col-md-4">
        <f:boxTitleAndToolWrap caption="Ga enak" icon="fa-magic" useClose="true" useCollapse="true"></f:boxTitleAndToolWrap>
        <f:boxContentWrap className="weekday" useSlimScroll="true" >
            ear (alias) fa-gears (alias) fa-gift fa-glass fa-globe fa-group (alias) fa-hdd-o fa-headphones fa-heart fa-heart-o fa-home fa-inbox fa-info fa-info-circle fa-key fa-keyboard-o fa-laptop fa-leaf fa-legal (alias) fa-lemon-o fa-level-down fa-level-up fa-lightbulb-o fa-location-arrow fa-lock fa-magic fa-magnet fa-mail-forward (alias) fa-mail-reply (alias) fa-mail-reply-all fa-male fa-map-marker fa-meh-o fa-microphone fa-microphone-slash fa-minus fa-minus-circle fa-minus-square fa-minus-square-o fa-mobile fa-mobile-phone (alias) fa-money fa-moon-o fa-music fa-pencil fa-pencil-square fa-pencil-square-o fa-phone fa-phone-square fa-picture-o fa-plane fa-plus fa-plus-circle fa-plus-square
        </f:boxContentWrap>
    </f:boxWrap>
</div>