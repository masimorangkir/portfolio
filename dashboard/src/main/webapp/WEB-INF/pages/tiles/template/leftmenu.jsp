<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="f" uri="/WEB-INF/tld/dashboard.tld" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c2" uri="http://java.sun.com/jstl/core_rt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<f:subMenuWrap caption="Report" icon="fa-dashboard" >
    <f:subMenuItem link="production">Production Report</f:subMenuItem>
    <f:subMenuItem link="sales">Sales Report</f:subMenuItem>
    <f:subMenuItem link="marketing">Marketing Report</f:subMenuItem>
</f:subMenuWrap>
<f:menuItem icon="fa-cogs" link='profile' idComponent="prof">Profile</f:menuItem>
<f:menuItem icon="fa-twitter" link='profile' idComponent="prof">User Preference</f:menuItem>
<f:subMenuWrap caption="Administrative Tools" icon="fa-keyboard-o">
    <f:subMenuItem link="addUser">User Management</f:subMenuItem>
    <f:subMenuItem link="emailSetting">Mass Email</f:subMenuItem>
</f:subMenuWrap>            