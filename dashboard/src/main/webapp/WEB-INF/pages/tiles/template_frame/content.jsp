<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="f" uri="/WEB-INF/tld/dashboard.tld" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>
<!DOCTYPE html>
<html>
    <head>
        <%-- Get value from Tiles {'name' attribute} and set it to page context {'id' attribute}--%>
        <tilesx:useAttribute id="inject" name="script" ignore="true"/>
        <tilesx:useAttribute id="injectHeading" name="heading" ignore="true"/>
        <tilesx:useAttribute id="injectHeading" name="heading" ignore="true"/>
        <tilesx:useAttribute id="injectBreadcrumb" name="breadcrumb" ignore="true"/>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
        <link type="text/css" rel="stylesheet" href="<s:url value="${inject}.css"/>"/>
        <title><tiles:insertAttribute name="title" ignore="true"/></title>
    </head>
    <body class="skin-black">
        <div class="container" id="main-container">
            <div id="main-content" >
                
                <c:if test="${not empty injectHeading}">
                <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="<tiles:insertAttribute name="logo" ignore="true"/>"></i>&nbsp<tiles:insertAttribute name="heading" ignore="true"/></h1>
                        <h4><tiles:insertAttribute name="description" ignore="true"/></h4>
                    </div>
                </div>
                <!-- END Page Title -->
                </c:if>
                    
                <c:if test="${not empty injectBreadcrumb}">
                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <tiles:insertAttribute name="breadcrumb" ignore="true"/>
                </div>
                <!-- END Breadcrumb -->
                </c:if>

                <!-- BEGIN Main Content -->
                <tiles:insertAttribute name="content" ignore="true"/>    
                <!-- END Main Content -->
            </div>

        </div>
        <script src="<s:url value="${inject}"/>.js"></script>
    </body>
</html>