<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="f" uri="/WEB-INF/tld/dashboard.tld" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>
<!DOCTYPE html>
<html>
    <head>
        <%-- Get value from Tiles {'name' attribute} and set it to page context {'id' attribute}--%>
        <tilesx:useAttribute id="inject" name="script" ignore="true"/>
        <tilesx:useAttribute id="injectHeading" name="heading" ignore="true"/>
        <tilesx:useAttribute id="injectHeading" name="heading" ignore="true"/>
        <tilesx:useAttribute id="injectBreadcrumb" name="breadcrumb" ignore="true"/>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
        
        <link type="text/css" rel="stylesheet" href="<s:url value="/assets/bootstrap/css/bootstrap.min.css"/>"/>
        <link type="text/css" rel="stylesheet" href="<s:url value="/assets/font-awesome/css/font-awesome.min.css"/>"/>
        <link type="text/css" rel="stylesheet" href="<s:url value="/assets/plugin/dataTables.bootstrap.css"/>"/>
        <link type="text/css" rel="stylesheet" href="<s:url value="/assets/beyond/beyond.min.css"/>"/>
        <link type="text/css" rel="stylesheet" href="<s:url value="/assets/main/beyond.animate.css"/>"/>
        <link type="text/css" rel="stylesheet" href="<s:url value="/assets/main/flaty.css"/>"/>
        <link type="text/css" rel="stylesheet" href="<s:url value="/assets/main/flaty-responsive.css"/>"/>
        <link type="text/css" rel="stylesheet" href="<s:url value="/assets/plugin/select2.css"/>"/>
        <link type="text/css" rel="stylesheet" href="<s:url value="/assets/plugin/select2-bootstrap.css"/>"/>
        <link type="text/css" rel="stylesheet" href="<s:url value="/assets/plugin/bootstrap-datepicker.css"/>"/>
        <link type="text/css" rel="stylesheet" href="<s:url value="/assets/plugin/daterangepicker-bs3.css"/>"/>
        <link type="text/css" rel="stylesheet" href="<s:url value="/modules/global-content/content.css"/>"/>
        
        
        <title><tiles:insertAttribute name="title" ignore="true"/></title>
    </head>
    <body class="skin-black">
        <div class="container" id="main-container">
            <div id="main-content" >
                
                <c:if test="${not empty injectHeading}">
                <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="<tiles:insertAttribute name="logo" ignore="true"/>"></i>&nbsp<tiles:insertAttribute name="heading" ignore="true"/></h1>
                        <h4><tiles:insertAttribute name="description" ignore="true"/></h4>
                    </div>
                </div>
                <!-- END Page Title -->
                </c:if>
                    
                <c:if test="${not empty injectBreadcrumb}">
                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <tiles:insertAttribute name="breadcrumb" ignore="true"/>
                </div>
                <!-- END Breadcrumb -->
                </c:if>

                <!-- BEGIN Main Content -->
                <tiles:insertAttribute name="content" ignore="true"/>    
                <!-- END Main Content -->
            </div>

        </div>
        <script src="<s:url value="/assets/jquery-stable/jquery.min.js"/>"></script>
        <script src="<s:url value="/assets/bootstrap/js/bootstrap.js"/>"></script>
        <script src="<s:url value="/assets/plugin/jquery.slimscroll.min.js"/>"></script>
        <script src="<s:url value="/assets/plugin/jquery.cookie.js"/>"></script>
        <script src="<s:url value="/assets/main/flaty.js"/>"></script>
        <script src="<s:url value="/assets/plugin/moment.min.js"/>"></script>
        <script src="<s:url value="/modules/global-content/content.js"/>"></script>
        <script src="<s:url value="/assets/plugin/dataTables.js"/>"></script>
        <script src="<s:url value="/assets/plugin/dataTables.bootstrap.js"/>"></script>
        <script src="<s:url value="/assets/plugin/jquery.number.js"/>"></script>
        <script src="<s:url value="/assets/plugin/select2.min.js"/>"></script>
        <script src="<s:url value="/assets/highchart/highcharts.js"/>"></script>
        <script src="<s:url value="/assets/plugin/bootstrap-datepicker.js"/>"></script>
        <script src="<s:url value="/assets/plugin/daterangepicker.js"/>"></script>
        <script src="<s:url value="/modules/production/production.js"/>"></script>
    </body>
</html>