<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page language="java"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="f" uri="/WEB-INF/tld/dashboard.tld" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>
<!DOCTYPE html>
<html>
    <head>
        <%-- Get value from Tiles {'name' attribute} and set it to page context {'id' attribute}--%>
        <tilesx:useAttribute id="inject" name="script" ignore="true"/>
        <tilesx:useAttribute id="defaultSrc" name="defaultLink" ignore="true"/>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
        <link type="text/css" rel="stylesheet" href="<s:url value="${inject}.css"/>"/>
        <title><tiles:insertAttribute name="title" ignore="true"/></title>
    </head>
    <body class="skin-blue">

        <div id="navbar" class="navbar navbar-fixed">
            <button type="button" class="navbar-toggle navbar-btn collapsed" data-toggle="collapse" data-target="#sidebar">
                <span class="fa fa-bars"></span>
            </button>
            <a class="navbar-brand" href="#">
                <small>
                    <tiles:insertAttribute name="logo" ignore="true"/>
                </small>
            </a>

            <!-- BEGIN Navbar Buttons -->
            <tiles:insertAttribute name="navbar" ignore="true"/> 
            <!-- END Navbar Buttons -->
        </div>

        <div class="container" id="main-container">
            <!-- BEGIN Sidebar -->
            <div id="sidebar" class="navbar-collapse collapse">


                <!-- BEGIN Navlist -->
                <ul class="nav nav-list" style="height: auto;">           
                    <tiles:insertAttribute name="leftmenu" ignore="true"/>
                </ul>
                <!-- END Navlist -->

                <!-- BEGIN Sidebar Collapse Button -->
                <div id="sidebar-collapse" class="visible-lg visible-md visible-sm">
                    <i class="fa fa-angle-double-left"></i>
                </div>
                <!-- END Sidebar Collapse Button -->
            </div>
            <!-- END Sidebar -->


            <!-- BEGIN Content -->
            <div id="main-content">
                <iframe scrolling="no" style="width:100%;min-height:100%" id="iContent" name="iContent" frameborder="0" src="<s:url value="${defaultSrc}"/>"> </iframe>
                <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#" style="display: none;"><i class="fa fa-chevron-up"></i></a>
                <a id="btn-scrolldown" class="btn btn-circle btn-lg" href="#" style="display: none;"><i class="fa fa-chevron-down"></i></a>
            </div>

            <!-- END Content -->
        </div>

        <script type="text/javascript" src="<s:url value="${inject}.js"/>"></script>               
        <%--<script type="text/javascript" src="<s:url value="/bundles/whitelabel.js"/>"></script>               --%>
    </body>
</html>
