<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<ul class="nav flaty-nav pull-right">

    <!-- BEGIN Button Tasks -->
    <li class="hidden-xs">
        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
            <i class="fa fa-tasks"></i>
            <span class="badge badge-warning">4</span>
        </a>

        <!-- BEGIN Tasks Dropdown -->
        <ul class="dropdown-navbar dropdown-menu">
            <li class="nav-header">
                <i class="fa fa-check"></i>
                4 Tasks to complete
            </li>

            <li>
                <a href="#">
                    <div class="clearfix">
                        <span class="pull-left">Software Update</span>
                        <span class="pull-right">75%</span>
                    </div>

                    <div class="progress progress-mini">
                        <div style="width:75%" class="progress-bar progress-bar-warning"></div>
                    </div>
                </a>
            </li>

            <li>
                <a href="#">
                    <div class="clearfix">
                        <span class="pull-left">Transfer To New Server</span>
                        <span class="pull-right">45%</span>
                    </div>

                    <div class="progress progress-mini">
                        <div style="width:45%" class="progress-bar progress-bar-danger"></div>
                    </div>
                </a>
            </li>

            <li>
                <a href="#">
                    <div class="clearfix">
                        <span class="pull-left">Bug Fixes</span>
                        <span class="pull-right">20%</span>
                    </div>

                    <div class="progress progress-mini">
                        <div style="width:20%" class="progress-bar"></div>
                    </div>
                </a>
            </li>

            <li>
                <a href="#">
                    <div class="clearfix">
                        <span class="pull-left">Writing Documentation</span>
                        <span class="pull-right">85%</span>
                    </div>

                    <div class="progress progress-mini progress-striped active">
                        <div style="width:85%" class="progress-bar progress-bar-success"></div>
                    </div>
                </a>
            </li>

            <li class="more">
                <a href="#">See tasks with details</a>
            </li>
        </ul>
        <!-- END Tasks Dropdown -->
    </li>
    <!-- END Button Tasks -->

    <li class="hidden-xs open">
        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
            <i class="fa fa-bell anim-swing"></i>
            <span class="badge badge-important">4</span>
        </a>

        <!-- BEGIN Notifications Dropdown -->
        <ul class="dropdown-navbar dropdown-menu">
            <li class="nav-header">
                <i class="fa fa-warning"></i>
                4 Notifications
            </li>

            <li class="notify">
                <a href="#">
                    <i class="fa fa-comment orange"></i>
                    <p>New Comments</p>
                    <span class="badge badge-warning">4</span>
                </a>
            </li>

            <li class="notify">
                <a href="#">
                    <i class="fa fa-twitter blue"></i>
                    <p>New Twitter followers</p>
                    <span class="badge badge-info">7</span>
                </a>
            </li>

            <li class="notify">
                <a href="#">
                    <i class="fa fa-bug pink"></i>
                    <p>New bug in program!</p>
                </a>
            </li>

            <li class="notify">
                <a href="#">
                    <i class="fa fa-shopping-cart green"></i>
                    <p>You have some new orders</p>
                    <span class="badge badge-success">+10</span>
                </a>
            </li>

            <li class="more">
                <a href="#">See all notifications</a>
            </li>
        </ul>
        <!-- END Notifications Dropdown -->
    </li>

    <!-- BEGIN Button User -->
    <li class="user-profile">
        <a data-toggle="dropdown" href="#" class="user-menu dropdown-toggle">
            <img class="nav-user-photo" src="<s:url value="/assets/image/avatar.png"/>" alt="Photo">
            <span id="user_info">
                ${getUserSession.name}
            </span>
            <i class="fa fa-caret-down"></i>
        </a>

        <!-- BEGIN User Dropdown -->
        <ul class="dropdown-menu dropdown-navbar" id="user_menu">
            <li class="nav-header">
                <i class="fa fa-clock-o"></i>
                Logined From <span class="login-time">${getUserSession.logTime.getTime()}</span>
            </li>

            <li>
                <a href="#">
                    <i class="fa fa-cog"></i>
                    Account Settings
                </a>
            </li>

            <li>
                <a href="#">
                    <i class="fa fa-user"></i>
                    Edit Profile
                </a>
            </li>

            <li>
                <a href="#">
                    <i class="fa fa-question"></i>
                    Help
                </a>
            </li>

            <li class="divider visible-xs"></li>

            <li class="visible-xs">
                <a href="#">
                    <i class="fa fa-tasks"></i>
                    Tasks
                    <span class="badge badge-warning">4</span>
                </a>
            </li>
            <li class="divider"></li>

            <li>
                <a href="<s:url value="/logout"/>">
                    <i class="fa fa-off"></i>
                    Logout
                </a>
            </li>
        </ul>
        <!-- BEGIN User Dropdown -->
    </li>
    <!-- END Button User -->
</ul>