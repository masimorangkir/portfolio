function loadChart(container, yAxisTitle, height, type, title, categories, series, allowNegative,useLegend) {
    var listColors = ['#0000dd', '#dd0000', '#21B6A8', '#87907D', '#ec6d66', '#177F75', '#B6212D', '#B67721', '#da2d8b', '#7F5417', '#FF8000',
        '#61e94c', '#FFAABF', '#91C3DC', '#FFCC00', '#E5E0C1', '#68BD66', '#179CE8', '#BBFF20', '#30769E', '#FFE500', '#C8E9FC', '#758a09',
        '#00CCFF', '#FFC080', '#4086AA', '#FFAABF', '#0000AA', '#AA6363', '#AA9900', '#1A8BC0', '#ECF8FF', '#758a09', '#dd3100', '#dea04a',
        '#af2a30', '#EECC99', '#179999', '#BBFF20', '#a92e03', '#dd9cc9', '#f30320', '#579108', '#ce9135', '#acd622', '#e46e46', '#53747d',
        '#36a62a', '#83877e', '#e82385', '#73f2f2', '#cb9fa4', '#12c639', '#f51b2b', '#985d27', '#3595d5', '#cb9987', '#d52192', '#695faf',
        '#de2426', '#295d5a', '#824b2d', '#08ccf6', '#e82a3c', '#fcd11a', '#2b4c04', '#3011fd', '#1df37b', '#af2a30', '#c456d1', '#dcf174',
        '#025df6', '#0ab24f', '#c0d962', '#62369f', '#73faa9'];
    var boolLegend = (useLegend==undefined)?true:useLegend;
    var minVal = null;
    if (!allowNegative)
        minVal = 0;
    Highcharts.setOptions({
        lang: {
            numericSymbols: [null, ' Mill', ' Bill']
        },
        colors: listColors
    });
    $(container).highcharts({
        chart: {
            type: type,
            height: height
        },
        legend: {
            enabled: boolLegend
        },
        credits: {
            text: '',
            href: ''
        },
        title: {
            text: title
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: categories,
            tickmarkPlacement: 'off'
        },
        yAxis: {
            min: minVal,
            title: {
                margin: 30,
                text: yAxisTitle
            }
        },
        /*      tooltip: {
         headerFormat: '<span style="font-size:10px">Year : <b>{point.key}</b></span><br/><div style="width:477px;">',
         pointFormat: '<div style="width:235px;float:left;padding-left:2px;font-size:8pt;">'+ 
         '<div style="float:left;width:5%;color:{series.color};background:{series.color};border-style:solid;border-width:1px;border-color:black;margin-right:3px;">. .</div>'+
         '<div style="float:left;width:30%;">{series.name}</div>'+
         '<div style="float:left;width:50%;text-align:right;font-weight:bold;">{point.y:,.2f}</div>'+
         '</div>',
         footerFormat: '</div>',
         shared: true,
         useHTML:true,
         hideDelay:10
         }, */
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: series
    });
}
function initSelect2(ctl, placeholder, address) {
    $(ctl).select2({
        blurOnChange: true,
        openOnEnter: false,
        minimumInputLength: 1,
        placeholder: placeholder,
        allowClear: true,
        initSelection: function(element, callback) {
            var data = {id: element.val(), text: element.val()};
            callback(data);
        },
        ajax: {
            url: address,
            dataType: 'json',
            data: function(term, page) {
                return {key: term}
            },
            results: function(data, page) {
                return {results: data, more: false};
            }
        }}
    )
}
function initRangePicker(el) {
    var attrName = "#" + $(el).attr("id");
    $(attrName).daterangepicker(
            {
      ranges: {
         'Today': [moment(), moment()],
         'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
         'Last 7 Days': [moment().subtract('days', 6), moment()],
         'Last 30 Days': [moment().subtract('days', 29), moment()],
         'This Month': [moment().startOf('month'), moment().endOf('month')],
         'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
      },
                //showDropdowns: true,
                startDate: moment().subtract('month', 1).startOf('month'),
                endDate: moment()
            },
    function(start, end) {
        $(el).find('span').html(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
    }
    );
    var dt = $(attrName).data("daterangepicker");
    $(el).find('span').html(dt.startDate.format('MMM D, YYYY') + ' - ' + dt.endDate.format('MMM D, YYYY'));
}
(function() {
    var parseLongToTime = function(lo) {
        var dt = new Date(parseInt(lo));
        return dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
    }, init = function() {
        $.extend({parseLongToTime: parseLongToTime});
        InitiateWidgets();
    };

    function maximize(n) {
        if (n) {
            var t = $(window).height(), i = n.find(".widget-header").height();
            n.find(".widget-body").height(t - i);
        }
    }
    function InitiateWidgets() {
        $('.widget-buttons *[data-toggle="maximize"]').on("click", function(n) {
            n.preventDefault();
            var t = $(this).parents(".widget").eq(0), i = $(this).find("i").eq(0), r = "fa-compress", u = "fa-expand";
            t.hasClass("maximized") ? (i && i.addClass(u).removeClass(r), t.removeClass("maximized"), t.find(".widget-body").css("height", "auto")) : (i && i.addClass(r).removeClass(u), t.addClass("maximized"), maximize(t))
            var zz = $(this).parents(".widget").find("div.map_reflow");
            if (zz.length != 0) {
                $(zz).highcharts().reflow()
            }
            ;
        });
        $('.widget-buttons *[data-toggle="collapse"]').on("click", function(n) {
            n.preventDefault();
            var t = $(this).parents(".widget").eq(0), r = t.find(".widget-body"), i = $(this).find("i"), u = "fa-plus", f = "fa-minus", e = 300;
            t.hasClass("collapsed") ? (i && i.addClass(f).removeClass(u), t.removeClass("collapsed"), r.slideUp(0, function() {
                r.slideDown(e)
            })) : (i && i.addClass(u).removeClass(f), r.slideUp(200, function() {
                t.addClass("collapsed")
            }))
        });
        $('.widget-buttons *[data-toggle="dispose"]').on("click", function(n) {
            n.preventDefault();
            var i = $(this), t = i.parents(".widget").eq(0);
            t.hide(300, function() {
                t.remove()
            })
        })
    }

    $(init);
}
)(jQuery)