var $myIframe = $('#iContent');
var myIframe = $myIframe[0];
function setIframeHeight() {
    $myIframe.height('auto');
    var newHeight = 0;
    if (myIframe.contentDocument) {
        newHeight = $(myIframe.contentDocument).height();
    } else if (myIframe.contentWindow) {
        newHeight = $(myIframe.contentWindow.document).height();
    }
    $myIframe.height(newHeight);
}
function setScroll(y){
    window.setTimeout( function(){ $("body").scrollTop(y)},20);
}

(function() {
    //to calculate the height of iframe's content
    //only need to call iframeAutoHeight once, before any attempt to load URL into iframe
    var init = function() {
        $.extend({setIframeHeight:setIframeHeight});
        $.extend({setScroll:setScroll});
//        $('iframe').iframeAutoHeight({
//            minHeight: 240, // Sets the iframe height to this value if the calculated value is less
//            heightOffset: 0 // Optionally add some buffer to the bottom
//        });
        $(".login-time").text(moment(parseInt($(".login-time").text())).format("HH:mm"));
        $("ul.nav-list li a").on("click", function(e) {
            //e.preventDefault();
            reset();
            if (!$(this).hasClass("dropdown-toggle")) {
                $(this).parent().addClass("active");
                $("#sidebar").collapse("hide");
                //reset();
//                $("iframe").css("height", "1px"); //to trigger iframeAutoHeight
            }
        });
        $("iframe").on("load", function() {
            window.setTimeout(setIframeHeight, 7000);
        });
        scrollDownHandler();
    },
            scrollDownHandler = function() {
                //--------------------- Go Top Button ---------------------//
                $(window).scroll(function() {
                    if ($(this).scrollTop() > 1) {
                        $('#btn-scrolldown').fadeIn();
                    } else {
                        $('#btn-scrolldown').fadeOut();
                    }
                });
                $('#btn-scrolldown').click(function() {
                    setIframeHeight();
                    $("html, body").animate({scrollTop: $(document).height()}, 10);
                    return false;
                });
            },
            reset = function() {
                $("ul.nav-list li a").each(function( ) {
                    if ($(this).parent().hasClass("active")) {
                        $(this).parent().removeClass("active");
                    }
                })
            };
    $(init);
})(jQuery)