(function() {
    var holder, cross, tblCompare, btnComparison, contextSelect2 = document.baseURI + "/search/";
    var idx = 0;
    var reloadComparison = function() {
        if (holder) {
            holder.ajax.reload();
        }
    };
    var init = function() {
        cross = window.parent.jQuery;
        tblCompare = $("#tbl_compare");
        btnComparison = $("#submitComparison");
        $(".control-search2").each(function() {
            var y = $(this);
            initSelect2(y, y.data("tips"), contextSelect2 + y.data("link"));
        });
        $(".control-date").datepicker({format: "dd-MM-yyyy"});
        $(".control-range").each(function() {
            initRangePicker($(this));
            var name = "#" + $(this).attr("id");
            $(name).on('hide.daterangepicker', function(ev, picker) {
                if (!$(this).hasClass("no-map")) {
                    var parentNode = $(this).parents(".period");
                    var defParam = {tipe: $(parentNode).data("param")};
                    var period = $(parentNode).find("button.btn-success").data("period");
                    var chartContainer = "#" + $(parentNode).parents(".widget-body").find(".map_reflow").attr("id");
                    $.extend(defParam, {period: period});
                    $.extend(defParam, {startDate: picker.startDate.format('YYYY-MM-DD')});
                    $.extend(defParam, {endDate: picker.endDate.format('YYYY-MM-DD')});
                    if ($(this).hasClass("top10")) {
                        chartzQtyTop(chartContainer, defParam);
                        return;
                    }
                    if ($(this).hasClass("price")) {
                        chartzPriceTop(chartContainer, defParam);
                        return;
                    }
                    chartzQty(chartContainer, defParam);
                }
            });
        });
        holder = $(tblCompare).DataTable({
            searching: false,
            lengthMenu: [[25, 50, 100, -1], [25, 50, 100, "All"]],
//            sdom: "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
//            dom: '<"top"i>rt<"bottom"flp><"clear">',
            dom: '<"pull-left"l><"pull-right"ip>rt<"bottom"lp><"clear">',
            language: {
                search: ' ... '
            },
            processing: true,
            serverSide: true,
            columns: [
                {data: 'piDate'},
                {data: 'soId'},
                {data: 'customerName'},
                {data: 'piId'},
                {data: 'priceIn'},
                {data: 'priceOut'},
                {data: 'qtyOut'},
                {data: 'uomDefault'},
                {data: 'inventoryId'},
                {data: 'typeId'}
            ],
            ajax: {
                url: "production/getGlobal",
                data: function(d) {
                    d.paramStart = $("#periodRange").data('daterangepicker').startDate.format('YYYY-MM-DD');
                    d.paramEnd = $("#periodRange").data('daterangepicker').endDate.format('YYYY-MM-DD');
                    d.paramPiid = $("#paramPiid").val();
                    d.paramSoid = $("#paramSoid").val();
                    d.paramCustomerName = $("#paramCustomerName").val();
                }
            },
            createdRow: function(row, data, index) {
                data.piDate = moment(parseInt(data.piDate)).format("DD-MMMM-YYYY");
                $("td", row).eq(0).html(data.piDate);
                $("td", row).eq(0).addClass("text-center");
                $("td", row).eq(3).addClass("text-left");
                $("td", row).eq(4).addClass("text-right");
                $("td", row).eq(5).addClass("text-right");
                $("td", row).eq(6).addClass("text-center");
                $("td", row).eq(8).addClass("text-center");
                $("td", row).eq(4).text($.number($("td", row).eq(4).text()));
                $("td", row).eq(5).text($.number($("td", row).eq(5).text()));
                if (!data.uomDefault) {
//                    $("td", row).eq(2).addClass("danger");
//                    for (var x = 4; x <= 8; x++) {
//                        $("td", row).eq(x).addClass("danger");
//                    }
                    // $(row).addClass("danger");
                }
            }
        }
        );
        $(tblCompare).on("draw.dt", function() {
            var loc = $("#mySearch").offset().top;
            if (cross.setIframeHeight) {
                cross.setIframeHeight();
                if(idx>0){cross.setScroll(loc)}; //handled by parent
            }else{
                if(idx>0){window.setTimeout( function(){ $("body").scrollTop(loc)},20)};
            }
            idx++;
        });
        btnComparison.on("click", reloadComparison);
        $(".period button").on("click", function(ev) {
            ev.preventDefault();

            var parentNode = $(this).parents(".period");
            $("button", parentNode).each(function() {
                $(this).removeClass("btn-success");
            });
            $(this).addClass("btn-success");
            var modeInOut = $(parentNode).data("param");
            var defParam = {tipe: modeInOut};
            var period = $(parentNode).find("button.btn-success").data("period");
            var chartContainer = "#" + $(parentNode).parents(".widget-body").find(".map_reflow").attr("id");
            var rangeId = "#" + $(this).prevAll(".control-range").attr("id")
            var rangeData = $(rangeId).data('daterangepicker');
            var options = {period: period, startDate: rangeData.startDate.format("YYYY-MM-DD"), endDate: rangeData.endDate.format("YYYY-MM-DD")};
            $.extend(defParam, options);
            if ($(this).hasClass("top10")) {
                chartzQtyTop(chartContainer, defParam);
                return;
            }
            if ($(this).hasClass("price")) {
                chartzPriceTop(chartContainer, defParam);
                return;
            }
            chartzQty(chartContainer, defParam, modeInOut);
            
        });
        $(".period button.btn-success").click();
    },
            sanitizeDate = function(val) {
                try {
                    return moment($(val).datepicker("getDate")).format("YYYY-MM-DD");
                }
                catch (err) {
                    return "";
                }
            },
            chartzPriceTop = function(container, input, title) {
                $.post("production/getPriceTopTen", input, function(obj) {
                    loadChart(container, "Price", 300, "bar", title, obj.chart.categories, obj.chart.series, false,false);
                });
            },
            chartzQtyTop = function(container, input, title) {
                $.post("production/getQtyTopTen", input, function(obj) {
                    loadChart(container, "Qty", 300, "bar", title, obj.chart.categories, obj.chart.series, false,false);
                });
            },
            chartzQty = function(container, input, title) {
                $.post("production/getAccQty", input, function(obj) {
                    loadChart(container, "Qty", 300, "spline", title, obj.categories, obj.series, false);
                });
            };

    $(init);
    $.extend({nope: sanitizeDate});
})(jQuery)