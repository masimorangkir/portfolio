/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dashboard.service;

import java.util.List;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author Genesys8
 */

@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(value = {"file:src/main/webapp/WEB-INF/dashboard-servlet.xml"})
@ContextConfiguration(value = "classpath:dashboard-test.xml")
public class IPIAjaxFetchServiceTest extends TestCase {
   
    @Autowired
    IPIAjaxFetchService piAjax;
    /**
     * Test of getAllSOCustomerName method, of class IPIAjaxFetchService.
     */
    @Test
    public void testGetAllSOCustomerName() {
        System.out.println("getAllSOCustomerName");
        List<String> result = piAjax.getAllSOCustomerName(null);
        for(String r:result){
            System.out.println(">>> "+r);
        }
    }

    /**
     * Test of getAllPIID method, of class IPIAjaxFetchService.
     */
    @Test
    public void testGetAllPIID() {
        System.out.println("getAllPIID");
        List<String> result = piAjax.getAllPIID(null);
        for(String r:result){
            System.out.println(">>> "+r);
        }
    }

    @Test
    public void testGetAllSOID() {
        System.out.println("getAllSOID");
        List<String> result = piAjax.getAllSOID(null);
        for(String r:result){
            System.out.println(">>> "+r);
        }
    }
    
}
