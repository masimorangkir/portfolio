/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dashboard.service;

import com.dashboard.common.model.HighchartMap;
import com.dashboard.common.model.HighchartSeries;
import com.dashboard.common.util.ReflectionUtil;
import com.dashboard.model.ChartPIQtyPrice;
import com.dashboard.model.PITableInfo;
import com.dashboard.model.ReportPIInOut;
import com.dashboard.model.ReportPIInOutWrapper;
import com.dashboard.param.ParamReportPIInOut;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:dashboard-test.xml")
public class IReportingPIServiceTest {

    @Autowired
    IReportingPIService rService;

    public IReportingPIServiceTest() {
    }

    /**
     * Test of getReportGlobal method, of class IReportingPIService.
     */
    @Test
    public void testGetReportGlobal() {
        System.out.println("getReportGlobal");
        ParamReportPIInOut paramReport = new ParamReportPIInOut();
        paramReport.setParamCustomerName("%");
        paramReport.setParamPiid("%");
        paramReport.setParamSoid("%");
        paramReport.setParamStart("2000-01-01");
        paramReport.setParamEnd("2015-01-01");
        paramReport.setParamSearch("");
        ReportPIInOutWrapper wrapper = rService.getReportGlobal(paramReport);
        for (ReportPIInOut reportPIInOut : wrapper.getListReports()) {
            System.out.println(ReflectionUtil.printGetMethodValues(reportPIInOut));
        }
    }

    @Test
    public void getChartQtyPriceByTypeAndKindTest() {
        System.out.println("getChartQtyPriceByTypeAndKindTest");
        String start = "2012-05-06";
        String end = "2012-05-07";
        List<ChartPIQtyPrice> lst = rService.getChartQtyPriceByTypeAndKind("in", "");
        List<ChartPIQtyPrice> lst2 = rService.getChartQtyPriceByTypeAndKind("out", "");
        for (ChartPIQtyPrice cq : lst) {
            System.out.println("IN >> " + ReflectionUtil.printPropertyValues(cq));
        }
        for (ChartPIQtyPrice cq : lst2) {
            System.out.println("OUT >> " + ReflectionUtil.printPropertyValues(cq));
        }
    }

    @Test
    public void getChartQtyInOutTest() {
        HighchartMap hc = rService.getChartQtyInOut("in", "", "m", "2010-01-01","2020-01-01");
        for (String cats : hc.getCategories()) {
            System.out.println("Category :: " + cats);
        }
        for (HighchartSeries ser : hc.getSeries()) {
            System.out.println("Series of :: " + ser.getName());
            int index = 1;
            for (Object val : ser.getData()) {
                System.out.println("\t" + index + ". Value :: " + new BigDecimal(val.toString()));
                index++;
            }

        }

    }

    /**
     * Test of getChartQtyTopTenByInventoryId method, of class
     * IReportingPIService.
     */
    @Test
    public void testGetChartQtyTopTenByInventoryId() {
        System.out.println("getChartQtyTopTenByInventoryId");
        String reportKind = "in";
        HighchartMap result = rService.getChartQtyTopTenByInventoryId(reportKind, null, null);
        System.out.println(ReflectionUtil.printPropertyValues(result));
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getChartPriceTopTenByInventoryId method, of class
     * IReportingPIService.
     */
    @Test
    public void testGetChartPriceTopTenByInventoryId() {
        System.out.println("getChartPriceTopTenByInventoryId");
        String reportKind = "in";
        String startDate = "2000-01-01";
        String endDate  = "2020-01-01";
        HighchartMap result = rService.getChartPriceTopTenByInventoryId(reportKind, startDate, endDate);
        System.out.println(ReflectionUtil.printPropertyValues(result));
        // TODO review the generated test code and remove the default call to fail.

    }

    /**
     * Test of getListQtyTopTenByInventoryName method, of class
     * IReportingPIService.
     */
    @Test
    public void testGetListQtyTopTenByInventoryName() {
        System.out.println("getListQtyTopTenByInventoryName");
        String reportKind = "in";
        List<PITableInfo> result = rService.getListQtyTopTenByInventoryName(reportKind, null, null);
        for (PITableInfo pap : result) {
            System.out.println(pap.getPrice());
            System.out.println(ReflectionUtil.printPropertyValues(pap));
        }
    }

    /**
     * Test of getListPriceTopTenByInventoryName method, of class
     * IReportingPIService.
     */
    @Test
    public void testGetListPriceTopTenByInventoryName() {
        System.out.println("getListPriceTopTenByInventoryName");
        String reportKind = "in";
        String startDate = "2000-01-01";
        String endDate = "2020-01-01";
        List<PITableInfo> result = rService.getListPriceTopTenByInventoryName(reportKind, startDate, endDate);
        for (PITableInfo pap : result) {
            System.out.println(pap.getPrice());
            System.out.println(ReflectionUtil.printPropertyValues(pap));
        }
        // TODO review the generated test code and remove the default call to fail.

    }
}
